

#include <stdio.h>
#include <stdlib.h>
#include<time.h>
#include<unistd.h>
#include<math.h>

typedef struct _matrix matrix;

struct _matrix {
  int rows;
  int cols;
  int * data;
};

/* Number generator:
 * -Refreshes seed every millisecond
 * -Consistently returns a value between [0-99]
 * -Values 0-99 occur equally often
 * -the argument of p = 1 gives the possibility of getting the return value 100
 * -Returns 100 roughly once every million executions (I have a fun little plan for this.)
*/
int randnum(int p){
  int x, n;
  n = RAND_MAX/100;
  x = rand();
  x = x / n;
  if(p != 1337){
    while(x == 100){
      x = rand();
      x = x / n;
    }
  }
  return x;
}

void setElement(matrix * mtx, int row, int col, int val){
  mtx->data[(col-1) * mtx->rows + row - 1] = val;
}

int getElement(matrix const *mtx, int row, int col){
  return mtx->data[(col-1) * mtx->rows + row - 1];
}

void printy(int c, int b, int x, int y){
  volatile double z, ret;
  //if((b-y) == 0) z = 100000000;
  //else z = (c - x)/(b - y); 
  float c1, b1, x1, y1;
  c1 = c;
  b1 = b;
  x1 = x;
  y1 = y;
  if ((b1 - y) == 0) z = 100000000;
  else z = (c1 - x1)/(b1 - y1);
  ret = atan (z);
  //printf(" %f", z);
  printf(" %0.1f", ret);
  //printf("%d ", (c - x)/(b - y));
  //if((b - y) == 0) f = 2147483647;
  //else f = 50*(c - x)/(b - y);
  //printf("%f", atan(f));
}
void printMatrix(matrix const * A, int matx, int maty, int x, int y, float dir){
  
  int b, i, c;
  
  
  dir++;
  if(dir > 4) dir -= 8;
  
  printf("+");
  for(c = 0; c <= maty; c++) printf("--");
  printf("+\n\r");
  for(b = matx; b > 0 ; b--){
    printf("|");
    for(c = 0; c <= maty; c++){
     i = getElement(A, c, b);
      if(c == x && b == y){
        //character direction when not in grass
          //printf(" ");
          //if( dir == 1) printf("^");
          //if( dir == 90) printf(">");
          //if( dir == 180) printf("v");
          //if( dir == 270) printf("<");
          printf("<>");
          continue;
        }


      if(dir == 0){
          if((c > x) && (b > y)) printy(c, b, x, y);
          else printf("  ");
      }
      if(dir == 2){
          if((c < x) && (b > y)) printy(c, b, x, y);
          else printf("  ");
      }
      if ((dir > -4) && (dir < -2)){
          if(((c/(dir + 4) + b/(-2 -dir)) < (x/(dir + 4) + y/(-2 - dir))) && (( b/(dir + 4) - c/(-2 -dir)) < (y/(dir + 4) - x/(-2 -dir)))) printy(c, b, x, y);
          else printf("  ");
      }
      if ((dir > -2) && (dir < 0)){
          if(((c/(dir + 2) + b/(0 - dir)) > (x/(dir + 2) + y/(0 - dir))) && (( b/(dir + 2) - c/(0 - dir)) < (y/(dir + 2) - x/(0 - dir)))) printy(c, b, x, y);
          else printf("  ");
      }
      if ((dir > 2) && (dir < 4)){
          if(((c/(dir - 2) + b/(4 - dir)) < (x/(dir - 2) + y/(4 - dir))) && (( b/(dir - 2) - c/(4 - dir)) > (y/(dir - 2) - x/(4 - dir)))) printy(c, b, x, y);
          else printf("  ");
      }
      if ((dir > 0) && (dir < 2)){
          if(((c/(dir) + b/(2 - dir)) > (x/(dir) + y/(2 - dir))) && (( b/(dir) - c/(2 - dir)) > (y/(dir) - x/(2 - dir)))) printy(c, b, x, y);
          else printf("  ");
      }
      if (dir == 4){
          if((c < x) && (b < y)) printy(c, b, x, y);
          else printf("  ");
      }
      if (dir == -2){
          if((c > x) && (b < y)) printy(c, b, x, y);
          else printf("  ");
      }
    //  if(((c/dir + b/(2-dir)) > (x + y)) && (( b/dir - c/(2-dir)) > (y - x))) printf(".");
    //  else printf(" ");

    }  
    printf("|\n\r");
  }
  printf("+");
  for(b = 0; b <= maty; b++) printf("--");
  printf("+\n\r");

  //printf("c = %d ", c);
  printf("dir = %f ", dir);
  //printf("c/dir = %f ", c/dir);
}

matrix * newMatrix(int rows, int cols){
  //basically copied this from the HW assignment
  int i;
  matrix * m;
  if (rows <=0 || cols <= 0) return NULL;
  m = (matrix *)malloc(sizeof(matrix));
  if(!m) return NULL;
  m->rows = rows;
  m->cols = cols;
  m->data = (int *) malloc(rows*cols*sizeof(int));
  if (!m->data) {
    free(m);
    return NULL;
  }
  for (i = 0; i < rows*cols; i++)
    m->data[i] = 0.0;
  return m;
}

void deleteMatrix(matrix * mtx){
  if(mtx) {
    free(mtx->data);
    free(mtx);
  }
}




#define CAMX 25  
#define CAMY 25  
#define ANGLE 1.2   
#define MATX 50
#define MATY 50
int main(){
int t = 0;
  t = clock() / (CLOCKS_PER_SEC / 1000);
  srand((unsigned) (long)(&t));
  //Start randomizer clock
  
 matrix * A;
  A = newMatrix(MATX + 8, MATY + 8); 
  //if(A==NULL) return -1;
/*
  int i = 1;
 while(i <= dim + 8){
    //looping for borders
    setElement(A, 1, i, 1);
    setElement(A, 2, i, 1);
    setElement(A, 3, i, 1);
    setElement(A, 4, i, 1);
    setElement(A, i, 1, 1);
    setElement(A, i, 2, 1);
    setElement(A, i, 3, 1);
    setElement(A, i, 4, 1);
    setElement(A, dim + 5, dim + 9 - i, 1);
    setElement(A, dim + 6, dim + 9 - i, 1);
    setElement(A, dim + 7, dim + 9 - i, 1);
    setElement(A, dim + 8, dim + 9 - i, 1);
    setElement(A, dim + 9 - i, dim + 5, 1);
    setElement(A, dim + 9 - i, dim + 6, 1);
    setElement(A, dim + 9 - i, dim + 7, 1);
    setElement(A, dim + 9 - i, dim + 8, 1);
    i++;
  }
  */
  float j = 0;
  char g;
  while (g != '.'){
    printMatrix(A, MATX, MATY, CAMX, CAMY, j);
    j = j - 1;
    if(j <= -4) j = 4;
  g = getchar();
  }
deleteMatrix(A); 



  return 0;
}
