#include <stdio.h>
#include <stdlib.h>
#include<time.h>
#include<unistd.h>
#include<math.h>


#define CAMX 22  
#define CAMY 22  
#define ANGLE 1.2   
#define MATX 44
#define MATY 44
#define SCRHI 60
#define SCRWI 60
#define SCXL 20
#define SCYL 20
#define VERTS 12
#define TRN 0.025
#define CROP 14
//CROP is the number of elements cut from each side 
////CROP = 1 means 6x6 -> 5x5
typedef struct _matrix matrix;

struct _matrix {
  int rows;
  int cols;
  int * data;
};

/* Number generator:
 * -Refreshes seed every millisecond
 * -Consistently returns a value between [0-99]
 * -Values 0-99 occur equally often
 * -the argument of p = 1 gives the possibility of getting the return value 100
 * -Returns 100 roughly once every million executions (I have a fun little plan for this.)
*/
int randnum(int p){
  int x, n;
  n = RAND_MAX/100;
  x = rand();
  x = x / n;
  if(p != 1337){
    while(x == 100){
      x = rand();
      x = x / n;
    }
  }
  return x;
}

void setElement(matrix * mtx, int row, int col, int val){
  mtx->data[(col-1) * mtx->rows + row - 1] = val;
}

int getElement(matrix const *mtx, int row, int col){
  return mtx->data[(col-1) * mtx->rows + row - 1];
}

void drawLines(matrix * C, int mht, int mwt, int inc){  
  int j = 0; 
  int k = 0;
  int b = 0;
  int c = 0;
  float x1 = 0;
  float y1 = 0;
  float x2 = 0;
  float y2 = 0;
  float x3 = 0;
  float y3 = 0;
  float xy = 0;
  float yx = 0;
  int l = 0;
  for(j = mht; j > 0; j--){
    for(k = 1; k < mwt; k++){
      if(getElement(C, j, k) > 2){
        x1 = k;
        y1 = j;
        //printf("#1 x = %d y = %d n = %d\n\r", k, j, getElement(C, j, k));
        for(b = mht; b > 0; b--){
          for(c = 1; c < mwt; c++){
            if((getElement(C, j, k) + 1 == getElement(C, b, c))){
              x2 = c;
              y2 = b;
              printf("#2 x = %d y = %d n = %d\n\r", c, b, getElement(C, b, c));
              
              if(x1 > x2) {
                x3 = x1;
                x1 = x2;
                x2 = x3;
                y3 = y1;
                y1 = y2;
                y2 = y3;
              }
              if(x1 != x2 && y1 != y2) yx = (y1 - y2)/(x1 - x2);
              if(x1 != x2 && y1 != y2) xy = (x1 - x2)/(y1 - y2);
              //printf("yx = %f, xy = %f\n\r", yx, xy);
                
              if(x2 == x1){
                  //printf("case -1 x%0.1fy%0.1f \n\r", x1, y1);
                  if (y2 > y1){
                    while(y1 < y2){
                      y1 = y1 + 1;
                      if(y1 != y2) setElement(C, roundf(y1), roundf(x1), inc);
                    }
                  }
                  else{
                    while(y1 > y2){
                      y1 = y1 - 1;
                      if(y1 != y2) setElement(C, roundf(y1), roundf(x1), inc);
                      }
                    }
                }
                if(y2 == y1){
                  //printf("case 0 x%0.1fy%0.1f \n\r", x1, y1);
                  while(x1 < x2){
                    x1 = x1 + 1;
                    if(x1 != x2) setElement(C, roundf(y1), roundf(x1), inc);
                  }
                }
              
              while((x1 != x2) && (y1 != y2)){ 
                  //printf("x%0.1fy%0.1f ", x1, y1);
      
                if(yx > 1){
                  y1 = y1 + 1;
                  x1 = x1 + xy;
                  //printf("case 1 x%0.1fy%0.1f \n\r", x1, y1);
                }
                if(yx == 1){
                  y1 = y1 + 1;
                  x1 = x1 + 1;
                  //printf("case 2 x%0.1fy%0.1f \n\r", x1, y1);
                }
                if((yx < 1) && (yx > 0)){
                  x1 = x1 + 1;
                  y1 = y1 + yx;
                  //printf("case 3 x%0.1fy%0.1f \n\r", x1, y1);
                }
                if((yx < 0) && (yx > -1)){
                  x1 = x1 + 1;
                  y1 = y1 + yx;
                  //printf("case 4 x%0.1fy%0.1f \n\r", x1, y1);
                }
                if(yx == -1){
                  x1 = x1 + 1;
                  y1 = y1 - 1;
                  //printf("case 5 x%0.1fy%0.1f \n\r", x1, y1);
                }
                if(yx < -1){
                  y1 = y1 - 1;
                  x1 = x1 - xy;
                  //printf("case 6 x%0.1fy%0.1f \n\r", x1, y1);
                }
                if((x1 != x2) && (y1 != y2)) setElement(C, roundf(y1), roundf(x1), inc);
              
              }
              l = 0;
              x1 = 0;
              x2 = 0;
              y1 = 0;
              y2 = 0;
              xy = 0;
              yx = 0;
            } 
          }
        }
      }
    }
  }
}
  

void printMatrix(matrix const * A, matrix * C, float matx, float maty, float x, float y, float dir, float tab){
  int b, c; 
  volatile int i;
  int g = 0;
  float var; 
  dir = dir * 3.14;
  tab = tab * 3.14;
  float inc = dir + 0.157;
  if (inc > 3.14) inc = inc - 6.28;
  //printf("+");//for(c = 0; c <= maty; c++) printf("--");//printf("+\n\r");//printf("dir = %f, x = %f, var = %f ", dir, (SCRHI - 0.5*(sqrt((c-x)*(c-x)+(b-y)*(b-y)))), var);
//if (dir <tab) printf("y = %f ", ( (var-dir)));//printf("c/dir = %f ", c/dir);//else printf("y = %f ", ( (tab-var)));//printf("etc = %f", sqrt((c-x)*(c-x)+(b-y)*(b-y)));
  for(b = maty; b > 1; b--){
    //printf("|");
    for(c = 1; c <= matx; c++){
      var = atan2(c - x, b - y);
      i = getElement(A, b, c);
      // ISSUE RIGHT HERE WITH I i = 0;
      if(c == x && b == y){
          //printf("<>");
          continue;
        }
     
      if (dir < tab){ 
        if ((var > dir) && (var < tab)){
          if(i > 0){
            printf("%d(x%0.1f y%0.1f) \n\r", i, round(SCRWI * (var-dir)/1.58 + 1), round((SCRHI + SCRHI/(sqrt((c-x)*(c-x)+(b-y)*(b-y))))/2)); 
            if(round(SCRWI * (var-dir)/1.58 + 1) == 0) setElement(C, round((SCRHI + SCRHI/(sqrt((c-x)*(c-x)+(b-y)*(b-y))))/2), round(SCRWI * (var-dir)/1.58 + 1 ), i); 
            else setElement(C, round((SCRHI + SCRHI/(sqrt((c-x)*(c-x)+(b-y)*(b-y))))/2), round(SCRWI * (var-dir)/1.58 + 1), i); 
            //setElement(C, 30, 40, 3);
          }
        }
      }
      else{ 
        if ((var > dir || (var < tab))){ 
          if(i > 0){
            if(var < 0){
              var = var + 6.28;
              if(dir < 0){ 
                dir = dir + 6.28;
                g = 1;
              }
            }
          printf("%d(x%0.1f y%0.1f) \n\r", i, round(SCRWI * (var-dir)/1.58 + 1), round((SCRHI +SCRHI/(sqrt((c-x)*(c-x)+(b-y)*(b-y))))/2)); 
          //setElement(C, 31, 41, 4);
          if(round(SCRWI * (var-dir)/1.58 + 1) == 0) setElement(C, round((SCRHI + SCRHI/(sqrt((c-x)*(c-x)+(b-y)*(b-y))))/2), round(SCRWI * (var-dir)/1.58 + 1), i); 
          else setElement(C, round((SCRHI +SCRHI/(sqrt((c-x)*(c-x)+(b-y)*(b-y))))/2), round(SCRWI * (var-dir)/1.58 + 1), i); 
          if(g == 1) dir = dir - 6.28;  
          }
        }
      }
    }    
  }
  drawLines(C, SCRHI, SCRWI, 1);
  
  printf("+");
  for(c = CROP + 1; c < SCRWI - CROP; c++) printf("--");
  printf("+\n\r");
  for(b = SCRHI - CROP; b >= SCRHI/2; b--){
    printf("|");
    for(c = 1 + CROP; c < SCRWI - CROP; c++){
      if((b < SCRHI) && (getElement(C, b + 1, c) > 1)) setElement(C, b, c, 2);
      if((b < SCRHI) && ((getElement(C, b + 1, c) == 1) || getElement(C, b + 1, c) == -1)) setElement(C, b, c, -1);
      if(getElement(C, b, c) > 0){
        if(getElement(C, b, c) == 2) printf(" |");
        else{ 
          if(getElement(C, b, c) < 10) printf(" %d", getElement(C, b, c));
          else printf("%d", getElement(C, b, c));
        }
      }
      else printf("  ");
    }
    printf("|\n\r");
  }
  for(b = SCRHI/2; b <= SCRHI - CROP; b++){
    printf("|");
    for(c = 1 + CROP; c < SCRWI - CROP; c++){
      if(getElement(C, b, c) > 0){
        if(getElement(C, b, c) == 2) printf(" |");
        else{ 
          if(getElement(C, b, c) < 10) printf(" %d", getElement(C, b, c));
          else printf("%d", getElement(C, b, c));
        }
      }
      else printf("  ");
    }
    printf("|\n\r");
  }
  printf("+");
  for(c = CROP + 1; c < SCRWI - CROP; c++) printf("--");
  for(b = SCRHI; b > 0; b--){
    for(c = 1; c < SCRWI; c++){
        setElement(C, b, c, 0);
    }
  }
  printf("+\n\r");


if (dir <tab) printf("x = %f ", round(SCRWI * abs(var-dir)/3.14));
  //printf("c/dir = %f ", c/dir);
else printf("x = %f ", round( SCRWI * abs(tab-var)/3.14));
//printf("etc = %f", 1/(1+sqrt((c-x)*(c-x)+(b-y)*(b-y))));
}

matrix * newMatrix(int rows, int cols){
  //basically copied this from the HW assignment
  int i;
  matrix * m;
  if (rows <=0 || cols <= 0) return NULL;
  m = (matrix *)malloc(sizeof(matrix));
  if(!m) return NULL;
  m->rows = rows;
  m->cols = cols;
  m->data = (int *) malloc(rows*cols*sizeof(int));
  if (!m->data) {
    free(m);
    return NULL;
  }
  for (i = 0; i < rows*cols; i++)
    m->data[i] = 0.0;
  return m;
}

void deleteMatrix(matrix * mtx){
  if(mtx) {
    free(mtx->data);
    free(mtx);
  }
}

int main(){

  system("/bin/stty raw");
  system("stty cbreak -echo");
  matrix * A;
  A = newMatrix(MATY + 1, MATX + 1);  //clean up plz
  matrix * C;
  C = newMatrix(SCRHI + 5, SCRWI + 5);
  //setE (Matrix, y, x #)
  //MATRIX DOES NOT HAVE 0TH ELEMENTS
  //can use full size elements
  int t = 0;
  for(t = 1; t <= MATY; t++) setElement(A, t, 1, -1);
  for(t = 2; t <= MATX; t++) setElement(A, 1, t, -1);
  for(t = 2; t <= MATY; t++) setElement(A, t, MATX, -1);
  for(t = 2; t <= MATX; t++) setElement(A, MATY, t, -1);

  setElement(A, 20, 20, 4);
  setElement(A, 20, 16, 3);
  //setElement(A, 16, 16, 4);
  setElement(A, 16, 20, 5);

  setElement(A, 28, 24, 7);
  //setElement(A, 28, 28, 8);
  setElement(A, 24, 24, 8);
  setElement(A, 24, 28, 9);

  setElement(A, 20, 28, 11);
  //setElement(A, 16, 28, 11);
  setElement(A, 20, 24, 12);
  setElement(A, 16, 24, 13);
  
  setElement(A, 32, 16, 19);
  //setElement(A, 28, 16, 16);
  setElement(A, 32, 20, 20);
  setElement(A, 36, 20, 21);
  
  setElement(A, 24, 16, 15);
  //setElement(A, 28, 16, 16);
  setElement(A, 24, 20, 16);
  setElement(A, 28, 20, 17);
  
  setElement(A, 32, 28, 23);
  //setElement(A, 28, 16, 16);
  setElement(A, 32, 24, 24);
  setElement(A, 36, 24, 25);
  
  
  
  drawLines(A, MATY, MATX, -1);
  int o;
  char g;
  g = getchar();
  t = clock() / (CLOCKS_PER_SEC / 1000);
  srand((unsigned) (long)(&t));
  float dir = -0.25;
  float tab = 0.25;
  int cx = CAMX;
  int cy = CAMY;
  while (g != '.'){
    for(o = MATY; o > 0; o--){
      for(t = 1; t <= MATX; t++){
        if(getElement(A, o, t) == 0) printf("  ");
        else printf("%d", getElement(A, o, t));
      }
      printf("\n\r");
    }
    printMatrix(A, C, MATX, MATY, cx, cy, dir, tab);
  printf("dir = %f", dir);
    g = getchar();
    if(g == 'w'){
      if((dir > -0.375) && (dir <= -0.125) && (getElement(A, cy + 1, cx) == 0)) cy++;
      if((dir > -0.125) && (dir <= 0.125) && (getElement(A, cy + 1, cx + 1) == 0) && (getElement(A, cy + 1, cx) == 0 || getElement(A, cy, cx + 1) == 0)){
        cx++;
        cy++;
      }
      if((dir > 0.125) && (dir <= 0.375) && (getElement(A, cy, cx + 1) == 0)) cx++;
      if((dir > 0.375) && (dir <= 0.625) && (getElement(A, cy - 1, cx + 1) == 0) && (getElement(A, cy - 1, cx) == 0 || getElement(A, cy, cx + 1) == 0)){
        cx++;
        cy--;
      }
      if((dir > 0.625) && (dir <= 0.875) && (getElement(A, cy - 1, cx) == 0)) cy--;
      if(((dir > 0.875) || (dir <= -0.875)) && (getElement(A, cy - 1, cx - 1) == 0) && (getElement(A, cy - 1, cx) == 0 || getElement(A, cy, cx - 1) == 0)){
        cx--;
        cy--;
      }
      if((dir > -0.875) && (dir <= -0.625) && (getElement(A, cy, cx - 1) == 0)) cx--;
      if((dir > -0.625) && (dir <= -0.375) && (getElement(A, cy + 1, cx - 1) == 0) && (getElement(A, cy + 1, cx) == 0 || getElement(A, cy, cx - 1) == 0)){
        cx--;
        cy++;
      }
    }
    if(g == 's'){
      if((dir > -0.375) && (dir <= -0.125) && (getElement(A, cy - 1, cx) == 0)) cy--;
      if((dir > -0.125) && (dir <= 0.125) && (getElement(A, cy - 1, cx - 1) == 0) && (getElement(A, cy - 1, cx) == 0 || getElement(A, cy, cx - 1) == 0)){
        cx--;
        cy--;
      }
      if((dir > 0.125) && (dir <= 0.375) && (getElement(A, cy, cx - 1) == 0)) cx--;
      if((dir > 0.375) && (dir <= 0.625) && (getElement(A, cy + 1, cx - 1) == 0) && (getElement(A, cy + 1, cx) == 0 || getElement(A, cy, cx - 1) == 0)){
        cx--;
        cy++;
      }
      if((dir > 0.625) && (dir <= 0.875) && (getElement(A, cy + 1, cx) == 0)) cy++;
      if(((dir > 0.875) || (dir <= -0.875)) && (getElement(A, cy + 1, cx + 1) == 0) && (getElement(A, cy + 1, cx) == 0 || getElement(A, cy, cx + 1) == 0)){
        cx++;
        cy++;
      }
      if((dir > -0.875) && (dir <= -0.625) && (getElement(A, cy, cx + 1) == 0)) cx++;
      if((dir > -0.625) && (dir <= -0.375) && (getElement(A, cy - 1, cx + 1) == 0) && (getElement(A, cy - 1, cx) == 0 || getElement(A, cy, cx + 1) == 0)){
        cx++;
        cy--;
      }
    }
    if(g == 'd'){
      if(dir > 1 - TRN) dir = -1;
      dir = dir + TRN;
      if(tab > 1 - TRN) tab = -1;
      tab = tab + TRN;
    }
    if(g == 'a'){
      if(dir < -1 + TRN) dir = 1;
      dir = dir - TRN;
      if(tab < -1 + TRN) tab = 1;
      tab = tab - TRN;
    }
    printf("%d = y, %d = x\n\r", cy, cx);
  }
deleteMatrix(A); 
deleteMatrix(C);
system ("stty cooked echo");
system ("/bin/stty cooked");

  return 0;
}
