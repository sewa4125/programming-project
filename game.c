#include<stdlib.h>
#include<stdio.h>
#include<time.h>
#include<unistd.h>
/* TO DO LIST
 * ---------------
 *  Add player spawn
 *  Add custom completeability check
 *  Add randomly generated maps
 *  Add randimized completability check
 *  fix prev best for score <100
 *  Maybe old school arcade style score keeper
 *  Remove Herobrine
 */

typedef struct _matrix matrix;

struct _matrix {
  int rows;
  int cols;
  int * data;
};

/* Number generator:
 * -Refreshes seed every millisecond
 * -Consistently returns a value between [0-99]
 * -Values 0-99 occur equally often
 * -the argument of p = 1 gives the possibility of getting the return value 100
 * -Returns 100 roughly once every million executions (I have a fun little plan for this.)
*/
int randnum(int p){
  int x, n;
  n = RAND_MAX/100;
  x = rand();
  x = x / n;
  if(p != 1337){
    while(x == 100){
      x = rand();
      x = x / n;
    }
  }
  return x;
}

void setElement(matrix * mtx, int row, int col, int val){
  mtx->data[(col-1) * mtx->rows + row - 1] = val;
}

int getElement(matrix const *mtx, int row, int col){
  return mtx->data[(col-1) * mtx->rows + row - 1];
}
  
void printMatrix(matrix const * A, matrix const * P, int w){
// This one is going to take some heavy editing. 
// W operates for unique circumstances, printing a portion of the matrix then cutting off to allow for information to display.
  int b, c;
  printf("+---------------------------+\n\r");
  for(b = 1; b <= 9; b++){
    printf("|");
    for(c = 1; c <= 9; c++){
      int i = getElement(A, getElement(P, 1, 1) - 5 + c, getElement(P, 1, 2) - 5 + b);
      if(c == 5 && b == 5){
        if( w == 9001){
          printf("!!!");
          //Battle encounter
          continue;
        }
        //character direction while in grass
        if(getElement(A, getElement(P, 1, 1), getElement(P, 1, 2)) == 2){
          printf(".");
          i = getElement(P, 1, 3);
          if( i == 1) printf("^");
          if( i == 2) printf(">");
          if( i == 3) printf("v");
          if( i == 4) printf("<");
          printf(".");
          continue;
        }
        //character direction when not in grass
        else{
          printf(" ");
          i = getElement(P, 1, 3);
          if( i == 1) printf("^");
          if( i == 2) printf(">");
          if( i == 3) printf("v");
          if( i == 4) printf("<");
          printf(" ");
          continue;
        }
      }
      //item display
      if( b == 6 && w == 3){
        printf("Items: W - Potion x %d      |\n\r", getElement(P, 1, 6));
        printf("|       A - Super Potion x %d|\n\r", getElement(P, 1, 7));
        printf("|       S - Fishing Rod     |\n\r");
        printf("|       E - Exit            |\r");
        b = 9;
        c = 9;
        continue;  
      }
      if( b == 9 && w == 4){
        printf("      Out of Potions       |\n\r");
        printf("+---------------------------+\n\r");
        return;
      }
      if( b == 9 && w == 5){
        printf(" It won't have any effect  |\n\r");
        printf("+---------------------------+\n\r");
        return;
      }
      if( b == 9 && w == 6){
        printf("   Out of Super Potions    |\n\r");
        printf("+---------------------------+\n\r");
        return;
      }
      //Returns with various amounts of the graph printed
      if( b == 7 && w == 7){
        return;
      }
      if( b == 9 && w == 8){
        return;
      }
      //printing the actual matrix
      
      if(i == 0) printf("   ");
      if(i == 1) printf("YVY");
      if(i == 3) printf(" o ");
      if(i == 4) printf(" o ");
      if(i == 5) printf("[|]");
      if(i == 9) printf("YVY");
      if(i == 2){
        int cnt = 0;
        int n;
        while(cnt !=3){
          n = b * c;
          n = n * cnt % 10;
          if (n <= 4)printf(".");
          if (n > 4) printf(",");
          cnt++;
          //little feature to make the grass more interesting
        }
      }
    }
    c = 1;  
    printf("|\n\r");
  }
  printf("+---------------------------+\n\r");
}

//popup when the item option is selected from the maze
void item(matrix * A, matrix * P){
  printMatrix(A, P, 3);
  int temp = 0;
  char g = 'y';
  char cont;
  while(g != 'w' && g != 'W' && g != 'a' && g != 'A' && g != 's' && g != 'S' && g != 'e' && g != 'E'){
    g = getchar();
  }
  if(g == 'w' || g == 'W'){
    if(getElement(P, 1, 6) == 0){
      temp = 1;
      printMatrix(A, P, 4);
      cont = getchar();
      //printmatrix saying how you are out of potions
      //Then either a continue or a break or something
      return;
    }
    if(getElement(P, 1, 4) == getElement(P, 1, 5)){
      temp = 1;
      printMatrix(A, P, 5);
      cont = getchar();
      //printmatrix saying how the potion will have no effect
      //Then either a continue or a break or something
    }
    if(temp == 0){
      temp = getElement(P, 1, 4);
      setElement(P, 1, 4, getElement(P, 1, 4) + 20);
      if(getElement(P, 1, 4) > getElement(P, 1, 5)){
        setElement(P, 1, 4, getElement(P, 1, 5));
      }
      setElement(P, 1, 6, getElement(P, 1, 6) -1);
      temp = getElement(P, 1, 4) - temp;
      printMatrix(A, P, 8);
      printf(" Dragonair recovered %d HP!|\n\r", temp);
      printf("+---------------------------+\n\r");
      cont = getchar();
      //used a Potion (Deincrement potion count and increase hp by 20 if possible)
    }
  }
  if(g == 'a' || g == 'A'){
    if(getElement(P, 1, 7) == 0){
      temp = 1;
      printMatrix(A, P, 6);
      cont = getchar();
      //printmatrix saying how you are out of super potions
      //Then either a continue or a break or something
      return; 
    }
    if(getElement(P, 1, 4) == getElement(P, 1, 5)){
      temp = 1;
      printMatrix(A, P, 5);
      cont = getchar();
      //printmatrix saying how the potion will have no effect
      //Then either a continue or a break or something
    }
    if(temp == 0){
      temp = getElement(P, 1, 4);
      setElement(P, 1, 4, getElement(P, 1, 4) + 50);
      if (getElement(P, 1, 4) > getElement(P, 1, 5)){
        setElement(P, 1, 4, getElement(P, 1, 5));
      }
      setElement(P, 1, 7, getElement(P, 1, 7) - 1);
      temp = getElement(P, 1, 4) - temp;
      printMatrix(A, P, 8);
      printf(" Dragonair recovered %d HP!|\n\r", temp);
      printf("+---------------------------+\n\r");
      cont = getchar(); 
      //used a Super Potion (Deincrement Super Potion count and add up to 50 to health)
    }
  }
  if(g == 's' || g == 'S'){
    printMatrix(A, P, 7);
    printf("Prof. Oak's words echoed...|\n\r");
    printf("|Theres a time and place for|\n\r");
    printf("|everything, but not now.   |\n\r");
    printf("+---------------------------+\n\r");
    cont = getchar();
    //silly little pokemon reference. 
    return;
  }
  if(g == 'e' || g == 'E'){
    return;
    //exit the items menu
  }
}

matrix * newMatrix(int rows, int cols){
  //basically copied this from the HW assignment
  int i;
  matrix * m;
  if (rows <=0 || cols <= 0) return NULL;
  m = (matrix *)malloc(sizeof(matrix));
  if(!m) return NULL;
  m->rows = rows;
  m->cols = cols;
  m->data = (int *) malloc(rows*cols*sizeof(int));
  if (!m->data) {
    free(m);
    return NULL;
  }
  for (i = 0; i < rows*cols; i++)
    m->data[i] = 0.0;
  return m;
}


int maptest(matrix  * A){
  int x, y, reach, orient, dir, counter;
  x = 5;
  y = 5; 
  counter = 0;
  orient = 1;
  dir = 1;


  while(reach != 5){
    if (dir == 1){
      while(reach != 1){
        reach = getElement(A, x + counter, y);
        if(reach == 5)break;
        printf("%d", reach);
        counter++;
      }
      counter = 0;
      dir++;
    }
    if (dir == 2){
    while(reach != 1){
      reach = getElement(A, x, y + counter);
      if(reach == 5)break;
      printf("%d", reach);
      counter++;
    }
    counter = 0;
    dir++;
    }
    if (dir == 3){
    while(reach != 1){
      reach = getElement(A, x - counter, y);
      if(reach == 5)break;
      printf("%d", reach);
      counter++;
    }
    counter = 0; 
    dir++;
    }
    if (dir == 4){
      while(reach != 1){
        reach = getElement(A, x, y - counter);
        if(reach == 5)break;
        printf("%d", reach);
        counter++;
      }
    counter = 0;
    dir++;
    }
/*
  if(orient == 1){
    if(getElement(A, x, y - 1) != 1){
      y--;
      orient = 4;
    continue;
    }
    else{
      if(getElement(A, x + 1, y) != 1){
        x++;
        continue;
      }
    }
  else orient = 2;
  if(orient == 2){
    if(getElement(A, x, y + 1) != 1)y++;
  }
  else orient = 3;
  if(orient == 3){
    if(getElement(A, x - 1, y) != 1)x--;
  }
  else orient = 4;
  if(orient == 4){
    if(getElement(A, x, y - 1) != 1)y--;
  }
  else return -2;
  */ 
  dir = 1;
  if(x == 5 && y == 5)return -1;
  }
  return 1;
}


void printBat(int p, char * e, int hp1, int hp, int LVL, int dhp1, int dhp, int s, int i){ 
  /* this is my feature that handles all of the display when a battle is ensuing.
      p = enemy level
      e = enemy name
      hp1 = enemy health remaining
      hp = total enemy health
      LVL = Dragonair level
      dhp1 = Dragonair remaining hp
      dhp = total Dragonair hp
      s = shiny true/false
      i = print feature(moves / items)
  */
  printf("+---------------------------+\n\r");
  if( s == 1){
    printf("|    ");
    if( *e == 'R') printf(" ");
    if( *e != 'M') printf("   ");
    printf("Lvl %d Shiny %s |\n\r", p, e);
  }
  else{
    printf("|          ");
    if( *e == 'R') printf(" ");
    if( *e != 'M') printf("   ");
    printf("Lvl %d %s |\n\r", p, e);
  }
  printf("|             HP %d/%d", hp1, hp);
  if(hp1 > 9){
    if(hp1 < 100){
      printf(" ");
    }
  }
  else{
    printf("  ");
  }
  if(hp < 100){
    printf(" ");
  }
  printf("    |\n\r");
  printf("|                           |\n\r");
  printf("| Lvl %d Dragonair          |\n\r", LVL);
  printf("| HP %d/%d", dhp1, dhp); 
  if(dhp1 > 9){
    if(dhp1 < 100){
      printf(" ");
    }
  }
  else{
    printf("  ");
  }
  printf("                |\n\r");
  if(i == 1){ 
    printf("|      W   -   Twister      |\n\r");
    printf("|      A   -   Slam         |\n\r");
    printf("|      S   -   Outrage      |\n\r");
  }
  return;
}

int battle(matrix const * A, matrix const * P, int s){
  //Battle feature! 
  //s is again the shiny variable
  int p = randnum(0);
  char * e;
  if (getElement(P, 1, 1) == 45 && getElement(P, 1, 2) == 35 && getElement(A, 15, 19) == 0 && getElement(A, 6, 39) == 0 && getElement(A, 45, 25) == 0) e = "Magikarp";
  else{
    if (p < 69){
      if(p < 9)  e = "Ralts";
      if(p >= 9 && p < 39) e = "Weedle";
      if(p >= 39 && p < 69) e = "Seedot";
    }
    else e = "Oddish";
  }
  printMatrix(A, P, 9001);
  char ent =getchar();
  printf("+---------------------------+\n\r");
  printf("|                           |\n\r");
  printf("|                           |\n\r");
  printf("|");
  if( s == 1){
    if( *e == 'R') printf(" ");
    if( *e != 'M') printf(" ");
    printf("A Shiny %s appeared! ", e);
    P->data[(0) * P->rows + 10] = getElement(P, 1, 11) + 1;
  }
  else {
    if( *e != 'M') printf(" ");
    printf(" A wild %s appeared! ", e);
    if( *e == 'R') printf(" ");
  }
  if( *e != 'M') printf(" ");
  printf("|\n\r");
  printf("|                           |\n\r");
  printf("|                           |\n\r");
  printf("|       Press any key       |\n\r");
  printf("|                           |\n\r");
  printf("|                           |\n\r");
  printf("|                           |\n\r");
  printf("+---------------------------+\n\r");
  ent = getchar();
  int LVL = getElement(P, 1, 13);
  int dhp1 = getElement(P, 1, 4);
  int dhp = getElement(P, 1, 5);
  int po = getElement(P, 1, 6);
  int po2 = getElement(P, 1, 7);
  //initialize Dragonair stats
  p = randnum(0)/10;
  p = p + 11; 
  if( *e == 'M') p = 100;
  int hp = 75 + p + p + randnum(0)/49;
  if( *e == 'R') hp = hp - 20;
  // Ralts has lower health while dealing more damage
  if( *e == 'O') hp = hp + 5;
  //Oddish has higher health
  if( *e == 'M') {
    hp = hp + 75;
    P->data[(0) * P->rows + 11]++;
  }
  //Magikarp is broke as hell.
  int hp1 = hp;
  int cnt;
  int temp = 0;
  while (hp1 > 0 && dhp1 > 0){
    printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
    printf("|      E   -   Items        |\n\r");
    printf("+---------------------------+\n\r");
    char u = getchar(); 
    while(u!='a' && u!='A' && u!='w' && u!='W' && u!='s' && u!='S' && u!='e' && u!='E'){
      u = getchar();
    }
    if( u == 'e' || u == 'E'){
      // Item option
      printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 0); 
      printf("| Items:                    |\n\r");
      printf("|     W - Potion        x %d |\n\r", po);
      printf("|     A - Super Potion  x %d |\n\r", po2);
      printf("|     E - Exit              |\n\r");
      printf("+---------------------------+\n\r");
      u = getchar();
      while( u != 'w' && u != 'W' && u != 'a' && u != 'A' && u != 'e' && u != 'E'){
        u = getchar();
      }
      temp = 0;
      if(u == 'w' || u == 'W'){
        if(po == 0){
          temp = 1;
          printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 0); 
          printf("| Items:                    |\n\r");
          printf("|     W - Potion        x %d |\n\r", po);
          printf("|     A - Super Potion  x %d |\n\r", po2);
          printf("| Out of Potions!           |\n\r");
          printf("+---------------------------+\n\r");
          cnt = getchar();
          //Returns to the start of the function so that you still get your turn since you didn't really accomplish anything.
          continue;
        }
        if(dhp1 == dhp){
          temp = 1;
          printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 0); 
          printf("| Items:                    |\n\r");
          printf("|     W - Potion        x %d |\n\r", po);
          printf("|     A - Super Potion  x %d |\n\r", po2);
          printf("| It won't have any effect. |\n\r");
          printf("+---------------------------+\n\r");
          cnt = getchar();
          //Also uses continue so that you have an actual turn.
          continue;
        }
        if(temp == 0){
          temp = dhp1;
          dhp1 = dhp1 + 20;
          if( dhp1 > dhp){
            dhp1 = dhp;
          }
          po--;
          temp = dhp1 - temp;
          printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 0); 
          printf("| Items:                    |\n\r");
          printf("|     W - Potion        x %d |\n\r", po);
          printf("|     A - Super Potion  x %d |\n\r", po2);
          if(temp > 9) printf("| Dragonair recovered %d HP!|\n\r", temp);
          else printf("| Dragonair recovered %d HP! |\n\r", temp);
          printf("+---------------------------+\n\r");
          u = 'n';  
          cnt = getchar();
          // progresses to the rest of the function so that regaining health counts as a move.
        }
      }
      if(u == 'a' || u == 'A'){
        if(po2 == 0){
          temp = 1;
          printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 0); 
          printf("| Items:                    |\n\r");
          printf("|     W - Potion        x %d |\n\r", po);
          printf("|     A - Super Potion  x %d |\n\r", po2);
          printf("| Out of Super Potions!     |\n\r");
          printf("+---------------------------+\n\r");
          cnt = getchar();
          //Nothing accomplished, try again! 
          continue;
        }
        if(dhp1 == dhp){
          temp = 1;
          printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 0); 
          printf("| Items:                    |\n\r");
          printf("|     W - Potion        x %d |\n\r", po);
          printf("|     A - Super Potion  x %d |\n\r", po2);
          printf("| It won't have any effect. |\n\r");
          printf("+---------------------------+\n\r");
          cnt = getchar();
          //The last ineffective option.
          continue;
        }
        if(temp == 0){
          temp = dhp1;
          dhp1 = dhp1 + 50;
          if (dhp1 > dhp){
            dhp1 = dhp;
          }
          po2--;
          temp = dhp1 - temp;
          printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 0); 
          printf("| Items:                    |\n\r");
          printf("|     W - Potion        x %d |\n\r", po);
          printf("|     A - Super Potion  x %d |\n\r", po2);
          if(temp > 9) printf("| Dragonair recovered %d HP!|\n\r", temp);
          else printf("| Dragonair recovered %d HP! |\n\r", temp);
          printf("+---------------------------+\n\r");
          u = 'n';  
          cnt = getchar();
          //recovered health counts as a move
        }
      }
      if(u == 'e' || u == 'E'){
        //exit item menu. Looking at your items shouldn't void your chance to attack.
        continue;
      }
    }  
    if( u == 'w' || u == 'W'){
      cnt = 4 * LVL - 120;
      //damage value to start at 40 then increase with level
      if( LVL != 40) cnt = cnt + randnum(0)/49;
      printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
      printf("| Dragonair used Twister!   |\n\r");
      printf("+---------------------------+\n\r");
      ent = getchar();
      hp1 = hp1 - cnt;
      if (hp1 < 0) hp1 = 0;
      printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
      printf("| %s took %d damage!", e, cnt);
      if( *e == 'R') printf(" ");
      if( *e != 'M') printf("  ");
      if(cnt < 100) printf(" ");
      printf(" |\n\r");
      printf("+---------------------------+\n\r");
      P->data[(0) * P->rows + 9] = getElement(P, 1, 10) + cnt;
      //I have to use these things instead of setElement because it REALLLY doesn't like having those in this function. 
      //Add damage done to total damage dealt
      ent = getchar();
    } 
    if(u == 'a' || u == 'A'){ 
      cnt = 2 * LVL;
      // base damage of 80 with a small increase (plus randomizer) for each level
      if( LVL != 40) cnt = cnt + randnum(0)/32;
      printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
      printf("| Dragonair used Slam!      |\n\r");
      printf("+---------------------------+\n\r");
      ent = getchar();
      if( randnum(0) < 105 - LVL - LVL){
        //chance of missing decreases with level
        printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
        printf("| The attack missed!        |\n\r");
        printf("+---------------------------+\n\r");
        ent = getchar();
      }
      else{
        hp1 = hp1 - cnt;
        if (hp1 < 0) hp1 = 0;
        printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
        printf("| %s took %d damage!", e, cnt);
        P->data[(0) * P->rows + 9] = getElement(P, 1, 10) + cnt;
        //add damage to total damage done
        if( *e == 'R') printf(" ");
        if( *e != 'M') printf("  ");
        if(cnt < 100) printf(" ");
        printf(" |\n\r");
        printf("+---------------------------+\n\r");
        ent = getchar();
      }
    }
    if (u == 's' || u == 'S'){
      cnt = LVL + 80;
      // Very strong move with slight increases in damage per level.
      if( LVL != 40) cnt = cnt + randnum(0)/25;
      printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
      printf("| Dragonair used Outrage!   |\n\r");
      printf("+---------------------------+\n\r");
      ent = getchar();
      if(randnum(0) < 130 - LVL - LVL){
        //also very inaccurate. Should get some levels.
      printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
        printf("| The attack missed!        |\n\r");
        printf("+---------------------------+\n\r");
        ent = getchar();
      }
      else{
        hp1 = hp1 - cnt;
        if (hp1 < 0) hp1 = 0;
        printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
        printf("| %s took %d damage!", e, cnt);
        P->data[(0) * P->rows + 9] = getElement(P, 1, 10) + cnt;
        //increment total damage by cnt
        if( *e == 'R') printf(" ");
        if( *e != 'M') printf("  ");
        if(cnt < 100) printf(" ");
        printf(" |\n\r");
        printf("+---------------------------+\n\r");
        ent = getchar();
      }
    }
    if(hp1 == 0) continue;
    //Fainted opponents don't get to attack!
    printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
    if( *e == 'R'){
      if(randnum(0) < 50){
        printf("| Ralts used Psychic!       |\n\r");
        printf("+---------------------------+\n\r");
        ent = getchar();
        if(randnum(0) < 75){
          cnt = 3 * p + randnum(0)/25;
          dhp1 = dhp1 - cnt;
          //Most powerful regularly occuring move in the game.
          //Can hit for over 60 hp.
          if (dhp1 < 0) dhp1 = 0;
          printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
          printf("| Dragonair took %d damage! |\n\r", cnt);
          printf("+---------------------------+\n\r");
          P->data[(0) * P->rows + 8] = getElement(P, 1, 9) + cnt;
          ent = getchar();
          //Bet that hurt didn't it, scrub?
        }
        else{
          printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
          printf("| The attack missed!        |\n\r");
          printf("+---------------------------+\n\r");
          ent = getchar();
        }
      }
      else{
        printf("| Ralts used Confusion!     |\n\r");
        printf("+---------------------------+\n\r");
        ent = getchar();
        cnt = p + randnum(0)/9;
        dhp1 = dhp1 - cnt;
        if (dhp1 < 0) dhp1 = 0;
        printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
        printf("| Dragonair took %d damage! |\n\r", cnt);
        printf("+---------------------------+\n\r");
        P->data[(0) * P->rows + 8] = getElement(P, 1, 9) + cnt;
        //add to damage taken
        ent = getchar();
      }
    }
    if( *e == 'S' && e[1] == 'e'){
      if(randnum(0) < 60){
          printf("| Seedot used Bullet Seed!  |\n\r");
          printf("+---------------------------+\n\r");
          ent = getchar();
        if(randnum(0) < 75){
          cnt =  p * randnum(0)/42 + 1;
          //large range of damage values, much like in the games
          cnt = cnt/2;
          dhp1 = dhp1 - cnt;
          if (dhp1 < 0) dhp1 = 0;
          printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
          printf("| Dragonair took %d damage! ", cnt);
          P->data[(0) * P->rows + 8] = getElement(P, 1, 9) + cnt;
          if( cnt < 10) printf(" ");
          printf("|\n\r+---------------------------+\n\r");
          ent = getchar();
        }
        else{
          printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
          printf("| The attack missed!        |\n\r");
          printf("+---------------------------+\n\r");
          ent = getchar();
        }
      }
      else{
        printf("| Seedot used Synthesis!    |\n\r");
        printf("+---------------------------+\n\r");
        ent = getchar();
        if(hp1 + 45 > hp) cnt = hp - hp1;
        else cnt = 45;
        hp1 = hp1 + cnt;
        printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
        //I initially had this set to be a full recovery. Stupid things wouldn't die.
        printf("| Seedot recovered %d HP!  ", cnt);
        if(cnt < 100){
          if(cnt < 10) printf("  ");
          else printf(" ");
        }
        printf("|\n\r+---------------------------+\n\r");
        ent = getchar();
      }
    }
    if( *e == 'W'){
      if(randnum(0) < 50){
        printf("| Weedle used Bug Bite!     |\n\r");
        printf("+---------------------------+\n\r");
        ent = getchar();
        if(randnum(0) < 75){
          cnt = p + randnum(0)/25;
          dhp1 = dhp1 - cnt;
          if (dhp1 < 0) dhp1 = 0;
          printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
          printf("| Dragonair took %d damage! ", cnt);
          P->data[(0) * P->rows + 8] = getElement(P, 1, 9) + cnt;
          if( cnt < 10) printf(" ");
          printf("|\n\r+---------------------------+\n\r");
          ent = getchar();
        }
        else{
          printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
          printf("| The attack missed!        |\n\r");
          printf("+---------------------------+\n\r");
          ent = getchar();
        }
      }
      else{
        printf("| Weedle used Poison Sting! |\n\r");
        printf("+---------------------------+\n\r");
        ent = getchar();
        cnt = randnum(0)/7;
        dhp1 = dhp1 - cnt;
        if (dhp1 < 0) dhp1 = 0;
        printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
        printf("| Dragonair took %d damage! ", cnt);
        if( cnt < 10) printf(" ");
        printf("|\n\r+---------------------------+\n\r");
        P->data[(0) * P->rows + 8] = getElement(P, 1, 9) + cnt;
        ent = getchar();
      }
    }
    if( *e == 'O' ){
      if( randnum(0) < 60){
        printf("| Oddish used Razor Leaf!   |\n\r");
        printf("+---------------------------+\n\r");
        ent = getchar();
        if(randnum(0) < 75){
          cnt =  p + randnum(0)/42;
          cnt = cnt/2;
          dhp1 = dhp1 - cnt;
          if (dhp1 < 0) dhp1 = 0;
          printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
          printf("| Dragonair took %d damage! ", cnt);
          P->data[(0) * P->rows + 8] = getElement(P, 1, 9) + cnt;
          if( cnt < 10) printf(" ");  
          printf("|\n\r+---------------------------+\n\r");
          ent = getchar();
        }
        else{
          printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
          printf("| The attack missed!        |\n\r");
          printf("+---------------------------+\n\r");
          ent = getchar();
        }
      }
      else{
        //This attack deals damage and slightly heals the user. 
        printf("| Oddish used Absorb!       |\n\r");
        printf("+---------------------------+\n\r");
        ent = getchar();
        cnt =  10 + randnum(0)/42;
        dhp1 = dhp1 - cnt;
        if (dhp1 < 0) dhp1 = 0;
        printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
        printf("| Dragonair took %d damage! ", cnt);
        if( cnt < 10) printf(" ");
        printf("|\n\r+---------------------------+\n\r");
        P->data[(0) * P->rows + 8] = getElement(P, 1, 9) + cnt;
        ent = getchar();
        if(hp1 + 5 > hp) cnt = hp - hp1;
        else cnt = 5;
        hp1 = hp1 + cnt;
        printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
        printf("| Oddish recovered %d HP!  ", cnt);
        if(cnt < 100){
          if(cnt < 10) printf("  ");
          else printf(" ");
        }
        printf("|\n\r+---------------------------+\n\r");
        ent = getchar();
      }
    }
    if( *e == 'M'){
      // U about 2 get rekt scrub.
      if(randnum(0) < 50){
        printf("| Magikarp used Splash!     |\n\r");
        printf("+---------------------------+\n\r");
        ent = getchar();
        printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
        printf("|It flops along the ground..|\n\r");
        printf("+---------------------------+\n\r");
        ent = getchar();
      }
      else{
        printf("| Magikarp used Outrage!    |\n\r");
        printf("+---------------------------+\n\r");
        
        ent = getchar();
        cnt = randnum(0)/7 + 145;
        //This is the most powerful attack in the whole game. 
        //Though you have to seek it out in the first place.
        dhp1 = dhp1 - cnt;
        if (dhp1 < 0) dhp1 = 0;
        printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
        printf("| Dragonair took %d damage!|\n\r", cnt);
        printf("+---------------------------+\n\r");
        //Owned pleb.
        P->data[(0) * P->rows + 8] = getElement(P, 1, 9) + cnt;
        ent = getchar();
        printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
        printf("| It's super effective!     |\n\r");
        printf("+---------------------------+\n\r");
        ent = getchar();
      }
    }
  }
  if(hp1 == 0){
    printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1);
    if(s == 1){
      printf("|");
      if( *e != 'M') printf(" ");
      printf("The Shiny %s fainted!", e);
      if( *e != 'M') printf(" ");
    }
    else{
      printf("| The wild %s fainted!", e);
      if( *e != 'M') printf("  ");
    }
    if( *e == 'R') printf(" ");
    //loads of if statements to make formatting look decent.
    printf("|\n\r+---------------------------+\n\r");
    ent = getchar();
    P->data[(0) * P->rows + 7]++;
    P->data[(0) * P->rows + 13]--;
    //Increment enemy KO counter
    //Deincrement level rollover thingy AKA KOs till next level
    if( *e == 'M'){
      A->data[(18) * A->rows + 14] = 3;
      A->data[(38) * A->rows + 5] = 4;
      A->data[(24) * A->rows + 44] = 4;
      A->data[(35) * A->rows + 43] = 3 + randnum(0)/50;
      A->data[(36) * A->rows + 44] = 3 + randnum(0)/50;
      A->data[(34) * A->rows + 44] = 3 + randnum(0)/50;
      A->data[(35) * A->rows + 45] = 3 + randnum(0)/50;
      P->data[(0) * P->rows + 13] = 0;
      P->data[(0) * P->rows + 12] += 9;
      //resets all of the items in the map and spawns a circle around the player
      //Ridiculous buffs if you actually manage to KO this freakin beast.
      dhp = dhp + 35;
      dhp1 = dhp1 + 35;
    }
    if(getElement(P, 1, 14) == 0){
      P->data[(0) * P->rows + 12]++;
      P->data[(0) * P->rows + 13] = getElement(P, 1, 13) - 35;
      cnt = randnum(0)/32 + 3;
      dhp1 = dhp1 + cnt;
      dhp = dhp + cnt;
      LVL = getElement(P, 1, 13);
      P->data[(0) * P->rows + 4] = dhp;
      printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
      printf("| Dragonair Leveled up!     |\n\r");
      printf("+---------------------------+\n\r");
      ent = getchar();
      //Leveling up increases total hp, remaining hp, and level (Thus attack damage and accuracy)
    }
  }
  else{
    printBat(p, e, hp1, hp, LVL, dhp1, dhp, s, 1); 
    printf("| Dragonair fainted!        |\n\r");
    printf("+---------------------------+\n\r");
    ent = getchar();
    //Lool nuub
  }
  P->data[(0) * P->rows + 3] = dhp1;
  P->data[(0) * P->rows + 5] = po;
  P->data[(0) * P->rows + 6] = po2;
  // Apply stats of battle to P matrix so they remain after this battle
  return 0;
}

int grass(matrix const * A, matrix const * P, int x){
  //chance of running into an enemy when stepping through grass
  int num = randnum(1337);
  if(num > 80){
    int b;
    if(num == 100 || getElement(P, 1, 15) == 1) b = battle(A, P, 1); 
    else(b = battle(A, P, 0));
  }
  return x;
  //Don't remember x actually being useful.
}

void deleteMatrix(matrix * mtx){
  if(mtx) {
    free(mtx->data);
    free(mtx);
  }
}
/*
 * Obligatory ASCII art.
__________________________________▓▓▓▓▓▓
_________________________▓▓▓▓▓▓░░░░░░░▓▓▓▓▓
_____________________▓▓▓▒▒▒▒▒▒▒░░░░░▓▓▓▓▓▓▓▓▓
______▒▒_________▓▓▓▓▓▓▒▒▒▒▒▒▒▒▒▒░░░░░▓___▓▓▓
____▒▒▒▒▒___▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▒▒▒▒▒▒▒░░░▓
___▒▒▒▒▒▒▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▒▒▒▒▓▒▒░▓▓
__▒▒▒▒▒▒▒▒▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▒▒▒▒▓▒▒▒▓▓
__▒▒▒▓▒▒▒▒▒▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▒▒▒▓▓▒▒▒▓
_▒▒▒▒▓▒▒▒▒▒▒▒▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▒▒▒▒▓▒▒▒▓
_▒▒▒▒▓▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▒▒▒▒▓▓▓▓▓
__▒▒▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▒▒▒▓_____▓
__▒▒▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▒▒▒▓
___▒▒▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▒▒▒▓▓▓▓▓▒▒▒▒▒▒▓
____▒▒▒▒▒▒▅▅▄▒▒▒▒▒▒▒▒▒▒▒▓▓▓▒▒▒▒▒▒▓▓▓▒▒▒▒▒▓▓
___▓░▒▒▒▒▅▅▅▅█▀▀▀▅▅▅▅▄▄▒▒▒▒▓▒█▒▒▄▒▒▒▄▅▓▒▒▒▒▒▓
___▓░░▒▒▒▒▄▅▅▌________▓_▀▀██▅▄█▒▒▀▀▀__█
__▓░░░░▓▒▒▒▒▒_________▓____▐██▒▒▒▒▒▌▓__█
__▓░░░░▓▒▒▒▒▒_________▓__▄███▌▒▒▒▒▐_▓██
__▓░░░░░▓▒▒▒▒▒________▓▓█__██▒▒▒▒▒▒▓▌▐
_▓▒▒░░░░░▓▒▒▒▒▒_______▓█████▒▒▒▒▒▒▒██
_▓▒▒░░░░░░▓▓▓▒▒▒▒_____▓███▀▒▒▒▒▒▒▒▒▒▒▒
_▓▒▒▒░░░░░░░░░▓▓▓▓▒▒__▓▒▒▒▒▒▒▒_▒▒▀▀▒▒
_▓▒▒▒▒░░░░░░░░░▓▓▒▒▒▒▒▒▒▒▒▒▒▒___▒▒▒▒
__▓▒▒▒▒▒░░░░▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒__▄▀
__▓▓▒▒▒▒▒░░░░░▓____▒▒▒▒▒▒▒▒▒▒▒▀
___▓▓▒▒▒▒▒▒░░░░░▓
___▓▓▓▒▒▒▒▒▒▒░░░░▓
____▓▓▓▒▒▒▒▒▒▒░░░░▓
____▓▓▓▓▓▒▒▒▒▒▒▒░░▓
______▓▓▓▓▓▒▒▒▒▒▒▒░▓
______▓▓▓▓▓▓▒▒▒▒__▓▓
________▓▓▓▓▓▓▒▒▓___▓
_________▓▓▓▓▓▓▒▒▓
__________▓▓▓▓▓▓▒▓
___________▓▓▓▓▓▓▓
____________▓▓▓▓▓
____________▓▓▓▓
___________▓▓▓
__________▓▓
*/
int main(){
  printf("+---------------------------+\n");
  printf("|                           |\n");
  printf("|                           |\n");
  printf("|      Pokemon Silicon      |\n");
  printf("|           V 1.22          |\n");
  printf("|                           |\n");
  printf("|       Press any key       |\n");
  printf("|                           |\n");
  printf("|                           |\n");
  printf("|                           |\n");
  printf("+---------------------------+\n");
  system ("/bin/stty raw");
  system ("stty cbreak -echo");
  char u;
  u=getchar();
  
  FILE * fp;
  int hiscr = 0;
  int score = 0;
  int t = 0;
  int i1 = 0;
  int i2 = 0;
  int dim = 42;
  int map = 0;
  int end = 0;
  
  t = clock() / (CLOCKS_PER_SEC / 1000);
  srand((unsigned) (long)(&t));
  //Start randomizer clock
  t = 0;

  while (end == 0){
  printf("+---------------------------+\n\r");
  printf("|                           |\n\r");
  printf("|            Map            |\n\r");
  printf("|                           |\n\r");
  printf("|    Standard       -  W    |\n\r");
  printf("|    Save 1");
  if(access ("save1.txt", F_OK ) != -1){
    printf("       ");
    t = 1;
  }
  else printf("(Empty)");
  printf("  -  A    |\n\r");
  printf("|    Save 2");
  if(access ("save2.txt", F_OK ) != -1){
    printf("       ");
    i1 = 1;
  }
  else printf("(Empty)");
  printf("  -  S    |\n\r");
  printf("|    Save 3");
  if(access ("save3.txt", F_OK ) != -1){
    printf("       ");
    i2 = 1;
  }
  else printf("(Empty)");
  printf("  -  D    |\n\r");
  printf("|                           |\n\r");
  printf("|                           |\n\r");
  printf("+---------------------------+\n\r");
  
 u = getchar();
 if( u == 'w' || u == 'W'){
 end = 1; 
  }
    if( u == 'a' || u == 'A'){
    if(t == 1){
  printf("+---------------------------+\n\r");
  printf("|                           |\n\r");
  printf("|           Save 1          |\n\r");
  printf("|                           |\n\r");
  printf("|     Play Current  - A     |\n\r");
  printf("|     Overwrite     - D     |\n\r");
  printf("|                           |\n\r");
  printf("|          Back - E         |\n\r");
  printf("|                           |\n\r");
  printf("|                           |\n\r");
  printf("+---------------------------+\n\r");
    u = getchar();
    while( u != 'a' && u != 'A' && u != 'd' && u != 'D' && u != 'e' && u != 'E'){
      u = getchar();
    }
    if( u == 'a' || u == 'A'){
      fp = fopen("save1.txt", "r");
      fscanf(fp, "%d %d", &dim, &hiscr); 
      //printf("%d", dim);
      fclose(fp);
      end = 1;
      map = 1;
    }
    if( u == 'd' || u == 'D'){
      remove("save1.txt");
      t--;
     
    }
  
    if( u == 'e' || u == 'E'){
      continue;
    }
    }
  if(t == 0){
  printf("+---------------------------+\n\r");
  printf("|                           |\n\r");
  printf("|           Save 1          |\n\r");
  printf("|         World Type        |\n\r");
  printf("|                           |\n\r");
  printf("|     Randomized    - W     |\n\r");
  printf("|     Custom 10x10  - A     |\n\r");
  printf("|     Custom 25x25  - S     |\n\r");
  printf("|     Custom 42x42  - D     |\n\r");
  printf("|         Back      - E     |\n\r");
  printf("+---------------------------+\n\r");
  u = getchar();
  while( u != 'w' && u != 'W' && u != 'a' && u != 'A' && u != 's' && u != 'S' && u != 'd' && u != 'D' && u != 'e' && u != 'E'){
    u = getchar();
  }
  if( u == 'w' || u == 'W'){
    dim = 7 + randnum(0)/2.3;
    map = 1;
    end = 1;
    t = 2;
  }
  if( u == 'a' || u == 'A'){
    dim = 10;
    map = 1;
    end = 1;
  }
  if( u == 's' || u == 'S'){
    dim = 25;
    map = 1;
    end = 1;
  }
  if( u == 'd' || u == 'D'){
    dim = 42;
    map = 1;
    end = 1;
  }
  if( u ==  'e' || u == 'E'){
    continue;
  }
  }

  }
  if( u == 's' || u == 'S'){
    if(i1 == 1){
  printf("+---------------------------+\n\r");
  printf("|                           |\n\r");
  printf("|           Save 2          |\n\r");
  printf("|                           |\n\r");
  printf("|     Play Current  - A     |\n\r");
  printf("|     Overwrite     - D     |\n\r");
  printf("|                           |\n\r");
  printf("|          Back - E         |\n\r");
  printf("|                           |\n\r");
  printf("|                           |\n\r");
  printf("+---------------------------+\n\r");
    u = getchar();
    while( u != 'a' && u != 'A' && u != 'd' && u != 'D' && u != 'e' && u != 'E'){
      u = getchar();
    }
    if( u == 'a' || u == 'A'){
      fp = fopen("save2.txt", "r");
      fscanf(fp, "%d %d", &dim, &hiscr); 
      //printf("%d", dim);
      fclose(fp);
      end = 1;
      map = 2;
    }
    if( u == 'd' || u == 'D'){
      remove("save2.txt");
      i1--;
     
    }
  
    if( u == 'e' || u == 'E'){
      continue;
    }
    }
  if(i1 == 0){
  printf("+---------------------------+\n\r");
  printf("|                           |\n\r");
  printf("|           Save 2          |\n\r");
  printf("|         World Type        |\n\r");
  printf("|                           |\n\r");
  printf("|     Randomized    - W     |\n\r");
  printf("|     Custom 10x10  - A     |\n\r");
  printf("|     Custom 25x25  - S     |\n\r");
  printf("|     Custom 42x42  - D     |\n\r");
  printf("|         Back      - E     |\n\r");
  printf("+---------------------------+\n\r");
  u = getchar();
  while( u != 'w' && u != 'W' && u != 'a' && u != 'A' && u != 's' && u != 'S' && u != 'd' && u != 'D' && u != 'e' && u != 'E'){
    u = getchar();
  }
  
  if( u == 'w' || u == 'W'){
    dim = 7 + randnum(0)/2.3;
    map = 2;
    end = 1;
    i1 = 2;
  }
  if( u == 'a' || u == 'A'){
    dim = 10;
    map = 2;
    end = 1;
  }
  if( u == 's' || u == 'S'){
    dim = 25;
    map = 2;
    end = 1;
  }
  if( u == 'd' || u == 'D'){
    dim = 42;
    map = 2;
    end = 1;
  }
  if( u ==  'e' || u == 'E'){
    continue;
  }
  }
  
  }
  if( u == 'd' || u == 'D'){
    if(i2 == 1){
  printf("+---------------------------+\n\r");
  printf("|                           |\n\r");
  printf("|           Save 3          |\n\r");
  printf("|                           |\n\r");
  printf("|     Play Current  - A     |\n\r");
  printf("|     Overwrite     - D     |\n\r");
  printf("|                           |\n\r");
  printf("|          Back - E         |\n\r");
  printf("|                           |\n\r");
  printf("|                           |\n\r");
  printf("+---------------------------+\n\r");
  u = getchar();
    while( u != 'a' && u != 'A' && u != 'd' && u != 'D' && u != 'e' && u != 'E'){
      u = getchar();
    }
    if( u == 'a' || u == 'A'){
      fp = fopen("save3.txt", "r");
      fscanf(fp, "%d %d", &dim, &hiscr); 
      //printf("%d", dim);
      fclose(fp);
      end = 1;
      map = 3;
    }
    if( u == 'd' || u == 'D'){
      remove("save3.txt");
      i2--;
     
    }
  
    if( u == 'e' || u == 'E'){
      continue;
    }
    }
  if(i2 == 0){
  printf("+---------------------------+\n\r");
  printf("|                           |\n\r");
  printf("|           Save 3          |\n\r");
  printf("|         World Type        |\n\r");
  printf("|                           |\n\r");
  printf("|     Randomized    - W     |\n\r");
  printf("|     Custom 10x10  - A     |\n\r");
  printf("|     Custom 25x25  - S     |\n\r");
  printf("|     Custom 42x42  - D     |\n\r");
  printf("|         Back      - E     |\n\r");
  printf("+---------------------------+\n\r");
  u = getchar();
  while( u != 'w' && u != 'W' && u != 'a' && u != 'A' && u != 's' && u != 'S' && u != 'd' && u != 'D' && u != 'e' && u != 'E'){
    u = getchar();
  }
  
  if( u == 'w' || u == 'W'){
    dim = 7 + randnum(0)/2.3;
    map = 3;
    end = 1;
    t = 2;
  }
  if( u == 'a' || u == 'A'){
    dim = 10;
    map = 3;
    end = 1;
  }
  if( u == 's' || u == 'S'){
    dim = 25;
    map = 3;
    end = 1;
  }
  if( u == 'd' || u == 'D'){
    dim = 42;
    map = 3;
    end = 1;
  }
  if( u ==  'e' || u == 'E'){
    continue;
  }
  }

  }

}
 
 matrix * A;
  A = newMatrix(dim + 8, dim + 8); 
  //if(A==NULL) return -1;
  int i = 1;
 while(i <= dim + 8){
    //looping for borders
    setElement(A, 1, i, 1);
    setElement(A, 2, i, 1);
    setElement(A, 3, i, 1);
    setElement(A, 4, i, 1);
    setElement(A, i, 1, 1);
    setElement(A, i, 2, 1);
    setElement(A, i, 3, 1);
    setElement(A, i, 4, 1);
    setElement(A, dim + 5, dim + 9 - i, 1);
    setElement(A, dim + 6, dim + 9 - i, 1);
    setElement(A, dim + 7, dim + 9 - i, 1);
    setElement(A, dim + 8, dim + 9 - i, 1);
    setElement(A, dim + 9 - i, dim + 5, 1);
    setElement(A, dim + 9 - i, dim + 6, 1);
    setElement(A, dim + 9 - i, dim + 7, 1);
    setElement(A, dim + 9 - i, dim + 8, 1);
    i++;
  }
  //Player matrix
  matrix * P;
  P = newMatrix(1, 16);
  setElement(P, 1, 1, 5);
  setElement(P, 1, 2, 5);
  setElement(P, 1, 3, 3);
  setElement(P, 1, 4, 325);
  setElement(P, 1, 5, 325);
  setElement(P, 1, 6, 2);
  setElement(P, 1, 7, 1);
  setElement(P, 1, 8, 0);
  setElement(P, 1, 9, 0);
  setElement(P, 1, 10, 0);
  setElement(P, 1, 11, 0);
  setElement(P, 1, 12, 0);
  setElement(P, 1, 13, 40);
  setElement(P, 1, 14, 4);
  setElement(P, 1, 15, 0);
  //Player values;
     // 1 - row(y), 
     // 2 - column(x),
     // 3 - direction(1=up, 2=right, 3=down, 4=left),
     // 4 - Dragonair health remaining
     // 5 - Dragonair total health
     // 6 - Number of Potions
     // 7 - Number of Super Potions
     // 8 - Number of enemies KOed
     // 9 - Damage Taken
     // 10- Damage Dealt
     // 11- Shinies Encountered
     // 12- Magikarp Fought
     // 13- Level
     // 14- XP counter
     // 15- Contra mode
 /* 
  end = 0;
  //conditions to break the loop
  */

  char c = 'y';
  int x, p;
  //temp things, not incredibly needed

  //randomizer
 /* if((t == 2) || (i1 == 2) || (i2 == 2)){
    x = 5; 
    p = 5;
 printf("%d",dim); 
    while(p < dim + 5){
      while(x < dim + 5){
 printf(" %d", p);
        if ((randnum(0)/(getElement(A, x - 2, p - 2)+ getElement(A, x - 2, p - 1) + getElement(A, x - 2, p) + getElement(A, x - 2, p + 1) + getElement(A, x - 2, p + 2) + getElement(A, x - 1, p - 2) + 2 * getElement(A, x - 1, p - 1) + 2 * getElement(A, x - 1, p) + 2 * getElement(A, x - 1, p + 1) + getElement(A, x - 1, p + 2) + 2 * getElement(A, x, p - 1) + 1)) < 5){
          setElement(A, x, p, 1);
        }
       x++;    
      }
     printf("\n\r");
     x = 5;
      p++;
    }
  setElement(A, dim, dim + 1, 2); 
  setElement(A, dim + 1, dim + 1, 2); 
  setElement(A, dim + 1, dim, 2); 
  setElement(A, dim, dim, 4); 

  } */
  x = 0;
  p = 0;


  if((map == 1 && t == 0) || (map == 2 && i1 == 0) || (map == 3 && i2 == 0)){
    end = 0;
    while(end !=1 && c != '.'){
    printMatrix(A, P, 0);
    c = getchar();
    if(c == 'd' || c == 'D'){
      setElement(P, 1, 3, 2);
      x = getElement(A, getElement(P, 1, 1) + 1, getElement(P, 1, 2));
      if(x != 1) setElement(P, 1, 1, getElement(P, 1, 1) + 1);
      //moving right
      
      //if(x == 0) setElement(P, 1, 1, getElement(P, 1, 1) + 1);
      //if(x == 5) setElement(P, 1, 1, getElement(P, 1, 1) + 1);
    
      }
    if(c == 's' || c == 'S'){
      setElement(P, 1, 3, 3);
      x = getElement(A, getElement(P, 1, 1), getElement(P, 1, 2) + 1);
      if(x != 1)  setElement(P, 1, 2, getElement(P, 1, 2) + 1);
      //if(x == 0) setElement(P, 1, 2, getElement(P, 1, 2) + 1);
      //if(x == 5) setElement(P, 1, 2, getElement(P, 1, 2) + 1);
        //moving down
    }
    if(c == 'a' || c == 'A'){
      //moving left
      setElement(P, 1, 3, 4);
      x = getElement(A, getElement(P, 1, 1) - 1, getElement(P, 1, 2));
      //if(x == 0) setElement(P, 1, 1, getElement(P, 1, 1) - 1);
      //if(x == 5) setElement(P, 1, 1, getElement(P, 1, 1) - 1);
      if(x != 1) setElement(P, 1, 1, getElement(P, 1, 1) - 1);
    }
    if(c == 'w' || c == 'W'){
      //moving up
      setElement(P, 1, 3, 1);
      x = getElement(A, getElement(P, 1, 1), getElement(P, 1, 2) - 1);
      //if(x == 0) setElement(P, 1, 2, getElement(P, 1, 2) - 1);
      //if(x == 5) setElement(P, 1, 2, getElement(P, 1, 2) - 1);
      if(x != 1) setElement(P, 1, 2, getElement(P, 1, 2) - 1);
    }
    if(c == 'e' || c == 'E'){
      p = getElement(A, getElement(P, 1, 1), getElement(P, 1, 2));
      p++;
      if (p == 1) p = 9;
      if (p == 10) p = 2;
      if (p == 6) p = 0;
      setElement(A, getElement(P, 1, 1), getElement(P, 1, 2), p);
    }
    if(c == 'r' || c == 'R'){
    }
  }
  }
/*printf("heir %d  ", map);
if(maptest(A) == -1){
  printf("error one");
}
if(maptest(A) == -2){
  printf("error two");
}
else{
  printf("Alright I think");
}
*/
if ((map != 0) && ((t == 0) || (i1 == 0) || (i2 == 0))){
 if (map == 1){
  fp = fopen("save1.txt", "w");
  }
  if (map == 2){
    fp = fopen("save2.txt", "w");
  }
  if (map == 3){
    fp = fopen("save3.txt", "w");
  }
  int m1 = 1; 
  int m2 = 1;
  int loc = 0;
  fprintf(fp, "%d %d            \n", dim, score);
  for(m1 = 1; m1 + 4 <= dim + 4; m1++){
    for(m2 = 1; m2 + 4 <= dim + 4; m2++){
    loc = getElement(A, m2 + 4, m1 + 4);
    if( loc == 9) loc = 1;
    fprintf(fp, " %d", loc);
    //printf(" %d", loc);
    }
    m2 = 0;
    fprintf(fp, "\n");
  }
  fclose(fp);

end = 0;
}
  
  int xx = 0;
  i1 = 0;
  i2 = 0;
  if(map == 0){
    fp = fopen("lmap.txt", "r");
    }
  if(map == 1){
    fp = fopen("save1.txt", "r");
    }
  if(map == 2){
    fp = fopen("save2.txt", "r");
    }
  if(map == 3){
    fp = fopen("save3.txt", "r");
    }
    fscanf(fp, "%d %d", &dim, &hiscr);
    while(fscanf(fp, "%d", &xx) != EOF){
    setElement(A, i1 + 5, i2 + 5, xx);
    i1++;
    if(i1 == dim){
      i1 = 0;
      i2++;
    } 
  }
  fclose(fp);

  c = 'i';
  printf("+---------------------------+\n\r");
  printf("|        Your Pokemon:      |\n\r");
  printf("|                           |\n\r");
  printf("| Dragonair:         Lvl 40 |\n\r");
  printf("|                HP 325/325 |\n\r");
  printf("|                           |\n\r");
  printf("| A mystical pokemon that   |\n\r");
  printf("| exudes a gentle aura.     |\n\r");
  printf("|                           |\n\r");
  printf("|      Press any key        |\n\r");
  printf("+---------------------------+\n\r");
  //description truncated from the actual one in a game
  u = getchar();
  //print game info
  printf("+---------------------------+\n\r");
  printf("|   Dragonair's Moveset:    |\n\r");
  printf("| Twister                   |\n\r");
  printf("|    40 Damage 100 Accuracy |\n\r");
  printf("| Slam                      |\n\r");
  printf("|    80 Damage 75 Accuracy  |\n\r");
  printf("| Outrage                   |\n\r");
  printf("|    120 Damage 50 Accuracy |\n\r");
  printf("|                           |\n\r");
  printf("|      Press any key        |\n\r");
  printf("+---------------------------+\n\r");
  u = getchar();
  printf("+---------------------------+\n\r");
  printf("|   Dragonair's Moveset:    |\n\r");
  printf("|                           |\n\r");
  printf("| Leveling up by battling   |\n\r");
  printf("| increases Health, Attack  |\n\r");
  printf("| Damage, and Attack        |\n\r");
  printf("| Accuracy!                 |\n\r");
  printf("|                           |\n\r");
  printf("|                           |\n\r");
  printf("|      Press any key        |\n\r");
  printf("+---------------------------+\n\r");
  u = getchar();
  printf("+---------------------------+\n\r");
  printf("|       Forest Info:        |\n\r");
  printf("| Trees             -   YVY |\n\r");
  printf("| Grass             -   ,., |\n\r");
  printf("| Items             -    o  |\n\r");
  printf("|                           |\n\r");
  printf("| Watch for rare and        |\n\r");
  printf("| powerful enemies!         |\n\r");
  printf("|                           |\n\r");
  printf("|      Press any key        |\n\r");
  printf("+---------------------------+\n\r");
  u = getchar();
  printf("+---------------------------+\n\r");
  printf("|         Controls          |\n\r");
  printf("|                           |\n\r");
  printf("|   W A S D  -   Movement   |\n\r");
  printf("|   E        -   Items      |\n\r");
  printf("|   R        -   Pokemon    |\n\r");
  printf("|   .        -   Quit       |\n\r");
  printf("|                           |\n\r");
  printf("|                           |\n\r");
  printf("|  Press any key to Begin!  |\n\r");
  printf("+---------------------------+\n\r");
  u=getchar();
  //Forest Construction



  //This is the rest of the matrix design.
  //And there is a LOT of it.
  //Skip to 2066 if this kind of stuff bores you.
  //columns are labeled and unique points are indented to stand out.
  //Really should have read from another file. Cause this sucked to make.
 /*
 //column 1 
  setElement(A, 5, 8, 1);
  setElement(A, 5, 9, 1);
  setElement(A, 5, 12, 1);
  setElement(A, 5, 15, 1);
  setElement(A, 5, 16, 1);
  setElement(A, 5, 17, 1);
  setElement(A, 5, 18, 1);
  setElement(A, 5, 20, 1);
  setElement(A, 5, 21, 1);
  setElement(A, 5, 22, 1);
  setElement(A, 5, 23, 1);
  setElement(A, 5, 26, 1);
  setElement(A, 5, 27, 1);
  setElement(A, 5, 28, 1);
  setElement(A, 5, 31, 1);
  setElement(A, 5, 32, 1);
  setElement(A, 5, 33, 1);
  setElement(A, 5, 38, 1);
  setElement(A, 5, 39, 1);
  setElement(A, 5, 40, 1);
  setElement(A, 5, 41, 1);
  setElement(A, 5, 42, 1);
  setElement(A, 5, 43, 1);
  setElement(A, 5, 46, 1);
  //column 2
  setElement(A, 6, 9, 1);
  setElement(A, 6, 10, 1);
  setElement(A, 6, 15, 2);
  setElement(A, 6, 16, 1);
  setElement(A, 6, 17, 2);
  setElement(A, 6, 21, 1);
  setElement(A, 6, 22, 1);
  setElement(A, 6, 27, 1); 
  setElement(A, 6, 30, 1);
  setElement(A, 6, 31, 1);
    setElement(A, 6, 39, 4);
  setElement(A, 6, 40, 1);
  setElement(A, 6, 41, 1);
  //column 3
  setElement(A, 7, 9, 1);
  setElement(A, 7, 10, 1);
  setElement(A, 7, 15, 2);
  setElement(A, 7, 16, 2);
  setElement(A, 7, 17, 2);
  setElement(A, 7, 22, 1);
  setElement(A, 7, 23, 1);
  setElement(A, 7, 25, 1);
  setElement(A, 7, 29, 1);
  setElement(A, 7, 30, 1);
  setElement(A, 7, 40, 1);
  setElement(A, 7, 45, 1);
  setElement(A, 7, 46, 1);
  //column 4
  setElement(A, 8, 14, 2);
  setElement(A, 8, 15, 2);
  setElement(A, 8, 16, 2);
  setElement(A, 8, 17, 2);
  setElement(A, 8, 18, 2);
  setElement(A, 8, 23, 1);
  setElement(A, 8, 24, 1);
  setElement(A, 8, 25, 2);
  setElement(A, 8, 29, 1);
  setElement(A, 8, 40, 1);
  setElement(A, 8, 41, 2);
  setElement(A, 8, 44, 1);
  setElement(A, 8, 46, 1);
  //column 5
  setElement(A, 9, 5, 1);
  setElement(A, 9, 14, 1);
  setElement(A, 9, 15, 1);
  setElement(A, 9, 16, 1);
  setElement(A, 9, 17, 1);
  setElement(A, 9, 18, 2);
  setElement(A, 9, 19, 2);
  setElement(A, 9, 23, 2);
  setElement(A, 9, 24, 2);
  setElement(A, 9, 25, 2);
  setElement(A, 9, 26, 2);
  setElement(A, 9, 30, 1);
  setElement(A, 9, 31, 1);
  setElement(A, 9, 39, 1);
  setElement(A, 9, 40, 2);
  setElement(A, 9, 41, 2);
  setElement(A, 9, 43, 1);
  setElement(A, 9, 44, 1);
  //column 6
  setElement(A, 10, 5, 1);
  setElement(A, 10, 6, 1);
  setElement(A, 10, 14, 1);
  setElement(A, 10, 15, 1);
  setElement(A, 10, 16, 1);
  setElement(A, 10, 17, 1);
  setElement(A, 10, 18, 1);
  setElement(A, 10, 19, 2);
  setElement(A, 10, 25, 2);
  setElement(A, 10, 26, 2);
  setElement(A, 10, 27, 2);
  setElement(A, 10, 28, 1);
  setElement(A, 10, 29, 1);
  setElement(A, 10, 30, 1);
  setElement(A, 10, 31, 2);
  setElement(A, 10, 38, 1);
  setElement(A, 10, 39, 1);
  setElement(A, 10, 40, 2);
  setElement(A, 10, 41, 2);
  setElement(A, 10, 42, 2);
  //column 7
  setElement(A, 11, 5, 1);
  setElement(A, 11, 6, 1);
  setElement(A, 11, 7, 1);
  setElement(A, 11, 10, 1);
  setElement(A, 11, 11, 2);
  setElement(A, 11, 12, 2);
  setElement(A, 11, 13, 2);
  setElement(A, 11, 14, 2);
  setElement(A, 11, 15, 2);
  setElement(A, 11, 16, 1);
  setElement(A, 11, 17, 1);
  setElement(A, 11, 18, 1);
  setElement(A, 11, 19, 1);
  setElement(A, 11, 20, 1);
  setElement(A, 11, 25, 2);
  setElement(A, 11, 26, 1);
  setElement(A, 11, 27, 1);
  setElement(A, 11, 28, 1);
  setElement(A, 11, 29, 1);
  setElement(A, 11, 30, 1);
  setElement(A, 11, 31, 2);
  setElement(A, 11, 32, 2);
  setElement(A, 11, 33, 1);
  setElement(A, 11, 37, 1);
  setElement(A, 11, 38, 1);
  setElement(A, 11, 39, 2);
  setElement(A, 11, 40, 2);
  setElement(A, 11, 41, 2);
  setElement(A, 11, 42, 2);
  setElement(A, 11, 43, 2);
  setElement(A, 11, 44, 2);
  setElement(A, 11, 45, 1);
  setElement(A, 11, 46, 1);
  //column 8
  setElement(A, 12, 6, 1);
  setElement(A, 12, 7, 1);
  setElement(A, 12, 8, 1);
  setElement(A, 12, 9, 1);
  setElement(A, 12, 10, 2);
  setElement(A, 12, 11, 2);
  setElement(A, 12, 12, 2);
  setElement(A, 12, 13, 2);
  setElement(A, 12, 14, 2);
  setElement(A, 12, 15, 2);
  setElement(A, 12, 16, 2);
  setElement(A, 12, 17, 1);
  setElement(A, 12, 18, 1);
  setElement(A, 12, 19, 1);
  setElement(A, 12, 20, 1);
  setElement(A, 12, 21, 1);
  setElement(A, 12, 22, 1);
  setElement(A, 12, 25, 1);
  setElement(A, 12, 26, 1);
  setElement(A, 12, 28, 1);
  setElement(A, 12, 29, 1);
  setElement(A, 12, 30, 2);
  setElement(A, 12, 31, 2);
  setElement(A, 12, 32, 1);
  setElement(A, 12, 34, 1);
  setElement(A, 12, 35, 1);
  setElement(A, 12, 36, 1);
  setElement(A, 12, 38, 1);
  setElement(A, 12, 39, 2);
  setElement(A, 12, 40, 2);
  setElement(A, 12, 41, 2);
  setElement(A, 12, 42, 1);
  setElement(A, 12, 43, 2);
  setElement(A, 12, 44, 2);
  setElement(A, 12, 45, 2);
  setElement(A, 12, 46, 1);
  //column 9
  setElement(A, 13, 7, 1);
  setElement(A, 13, 8, 1);
  setElement(A, 13, 9, 2);
  setElement(A, 13, 10, 2);
  setElement(A, 13, 11, 2);
  setElement(A, 13, 12, 2);
  setElement(A, 13, 15, 2);
  setElement(A, 13, 16, 2);
  setElement(A, 13, 17, 2);
  setElement(A, 13, 18, 1);
  setElement(A, 13, 19, 1);
  setElement(A, 13, 20, 1);
  setElement(A, 13, 21, 1);
  setElement(A, 13, 28, 1);
  setElement(A, 13, 29, 1);
  setElement(A, 13, 30, 2);
  setElement(A, 13, 31, 2);
  setElement(A, 13, 32, 1);
  setElement(A, 13, 40, 2);
  setElement(A, 13, 41, 1);
  setElement(A, 13, 42, 1);
  setElement(A, 13, 43, 2);
  setElement(A, 13, 44, 2);
  setElement(A, 13, 45, 1);
  setElement(A, 13, 46, 1);
  //column 10
  setElement(A, 14, 5, 1);
  setElement(A, 14, 8, 2);
  setElement(A, 14, 9, 2);
  setElement(A, 14, 10, 2);
  setElement(A, 14, 16, 2);
  setElement(A, 14, 17, 1);
  setElement(A, 14, 18, 1);
  setElement(A, 14, 19, 1);
  setElement(A, 14, 20, 1);
  setElement(A, 14, 27, 1);
  setElement(A, 14, 28, 1);
  setElement(A, 14, 31, 2);
  setElement(A, 14, 32, 1);
  setElement(A, 14, 33, 1);
  setElement(A, 14, 40, 1);
  setElement(A, 14, 41, 1);
  setElement(A, 14, 44, 2);
  setElement(A, 14, 45, 2);
  setElement(A, 14, 46, 1);
  //column 11
  setElement(A, 15, 17, 1);
  setElement(A, 15, 18, 1);
    setElement(A, 15, 19, 3);
  setElement(A, 15, 20, 1);
  setElement(A, 15, 27, 1);
  setElement(A, 15, 28, 1);
  setElement(A, 15, 32, 1);
  setElement(A, 15, 33, 1);
  setElement(A, 15, 34, 1);
  setElement(A, 15, 40, 1);
  setElement(A, 15, 41, 1);
  setElement(A, 15, 44, 1);
  setElement(A, 15, 45, 1);
  setElement(A, 15, 46, 1);
  //column 12
  setElement(A, 16, 16, 1);
  setElement(A, 16, 17, 1);
  setElement(A, 16, 20, 1);
  setElement(A, 16, 21, 1);
  setElement(A, 16, 22, 2);
  setElement(A, 16, 26, 1);
  setElement(A, 16, 27, 1);
  setElement(A, 16, 28, 2);
  setElement(A, 16, 32, 1);
  setElement(A, 16, 34, 1);
  setElement(A, 16, 35, 1);
  setElement(A, 16, 38, 1);
  setElement(A, 16, 39, 1);
  setElement(A, 16, 40, 1);
  setElement(A, 16, 41, 1);
  setElement(A, 16, 45, 1);
  setElement(A, 16, 46, 1);
  //column 13
  setElement(A, 17, 6, 1);
  setElement(A, 17, 7, 1);
  setElement(A, 17, 15, 1);
  setElement(A, 17, 16, 1);
  setElement(A, 17, 21, 1);
  setElement(A, 17, 22, 1);
  setElement(A, 17, 23, 2);
  setElement(A, 17, 27, 1);
  setElement(A, 17, 28, 2);
  setElement(A, 17, 29, 2);
  setElement(A, 17, 30, 2);
  setElement(A, 17, 31, 1);
  setElement(A, 17, 32, 1);
  setElement(A, 17, 35, 1);
  setElement(A, 17, 36, 2);
  setElement(A, 17, 39, 1);
  setElement(A, 17, 40, 1);
  setElement(A, 17, 44, 1);
  setElement(A, 17, 45, 1);
  setElement(A, 17, 46, 1);
  //column 14
  setElement(A, 18, 7, 1);
  setElement(A, 18, 8, 1);
  setElement(A, 18, 11, 1);
  setElement(A, 18, 12, 1);
  setElement(A, 18, 21, 1);
  setElement(A, 18, 22, 1);
  setElement(A, 18, 23, 2);
  setElement(A, 18, 24, 2);
  setElement(A, 18, 25, 2);
  setElement(A, 18, 26, 1);
  setElement(A, 18, 27, 2);
  setElement(A, 18, 28, 2);
  setElement(A, 18, 29, 2);
  setElement(A, 18, 30, 1);
  setElement(A, 18, 31, 1);
  setElement(A, 18, 32, 1);
  setElement(A, 18, 34, 1);
  setElement(A, 18, 35, 1);
  setElement(A, 18, 36, 2);
  setElement(A, 18, 37, 2);
  setElement(A, 18, 38, 2);
  setElement(A, 18, 40, 1);
  setElement(A, 18, 44, 2);
  setElement(A, 18, 45, 1);
  setElement(A, 18, 46, 1);
  //column 15
  setElement(A, 19, 5, 1);
  setElement(A, 19, 8, 1);
  setElement(A, 19, 9, 1);
  setElement(A, 19, 10, 1);
  setElement(A, 19, 18, 1);
  setElement(A, 19, 21, 1);
  setElement(A, 19, 22, 1);
  setElement(A, 19, 23, 2);
  setElement(A, 19, 24, 2);
  setElement(A, 19, 25, 2);
  setElement(A, 19, 26, 2);
  setElement(A, 19, 27, 2);
  setElement(A, 19, 28, 2);
  setElement(A, 19, 29, 2);
  setElement(A, 19, 30, 1);
  setElement(A, 19, 31, 1);
  setElement(A, 19, 32, 1);
  setElement(A, 19, 35, 1);
  setElement(A, 19, 36, 2);
  setElement(A, 19, 37, 2);
  setElement(A, 19, 38, 1);
  setElement(A, 19, 40, 1);
  setElement(A, 19, 41, 1);
  setElement(A, 19, 42, 2);
  setElement(A, 19, 43, 2);
  setElement(A, 19, 44, 2);
  setElement(A, 19, 45, 2);
  setElement(A, 19, 46, 1);
  //column 16
  setElement(A, 20, 5, 1);
  setElement(A, 20, 8, 1);
  setElement(A, 20, 9, 1);
  setElement(A, 20, 10, 1);
  setElement(A, 20, 11, 1);
  setElement(A, 20, 17, 1);
  setElement(A, 20, 18, 1);
  setElement(A, 20, 19, 1);
  setElement(A, 20, 21, 1);
  setElement(A, 20, 22, 1);
  setElement(A, 20, 23, 1);
  setElement(A, 20, 24, 2);
  setElement(A, 20, 25, 2);
  setElement(A, 20, 26, 2);
  setElement(A, 20, 27, 2);
  setElement(A, 20, 28, 2);
  setElement(A, 20, 29, 2);
  setElement(A, 20, 30, 1);
  setElement(A, 20, 31, 1);
  setElement(A, 20, 35, 1);
  setElement(A, 20, 36, 2);
  setElement(A, 20, 37, 2);
  setElement(A, 20, 38, 2);
  setElement(A, 20, 39, 1);
  setElement(A, 20, 40, 1);
  setElement(A, 20, 41, 2);
  setElement(A, 20, 42, 2);
  setElement(A, 20, 43, 2);
  setElement(A, 20, 44, 2);
  setElement(A, 20, 45, 2);
  setElement(A, 20, 46, 2);
  //column 17
  setElement(A, 21, 7, 1);
  setElement(A, 21, 8, 1);
  setElement(A, 21, 9, 1);
  setElement(A, 21, 10, 1);
  setElement(A, 21, 16, 1);
  setElement(A, 21, 17, 1);
  setElement(A, 21, 18, 1);
  setElement(A, 21, 19, 1);
  setElement(A, 21, 20, 1);
  setElement(A, 21, 21, 1);
  setElement(A, 21, 22, 1);
  setElement(A, 21, 23, 1);
  setElement(A, 21, 24, 1);
  setElement(A, 21, 25, 2);
  setElement(A, 21, 26, 2);
  setElement(A, 21, 27, 2);
  setElement(A, 21, 28, 2);
  setElement(A, 21, 29, 2);
  setElement(A, 21, 30, 1);
  setElement(A, 21, 31, 1);
  setElement(A, 21, 32, 1);
  setElement(A, 21, 38, 2);
  setElement(A, 21, 39, 1);
  setElement(A, 21, 40, 1);
  setElement(A, 21, 41, 1);
  setElement(A, 21, 42, 2);
  setElement(A, 21, 43, 2);
  setElement(A, 21, 44, 2);
  setElement(A, 21, 45, 2);
  setElement(A, 21, 46, 2);
  //column 18
  setElement(A, 22, 6, 1);
  setElement(A, 22, 7, 1);
  setElement(A, 22, 8, 1);
  setElement(A, 22, 9, 2);
  setElement(A, 22, 19, 1);
  setElement(A, 22, 20, 1);
  setElement(A, 22, 23, 1);
  setElement(A, 22, 24, 1);
  setElement(A, 22, 25, 1);
  setElement(A, 22, 26, 1);
  setElement(A, 22, 27, 2);
  setElement(A, 22, 28, 2);
  setElement(A, 22, 29, 1);
  setElement(A, 22, 30, 1);
  setElement(A, 22, 31, 1);
  setElement(A, 22, 32, 1);
  setElement(A, 22, 38, 1);
  setElement(A, 22, 39, 1);
  setElement(A, 22, 40, 1);
  setElement(A, 22, 41, 2);
  setElement(A, 22, 42, 2);
  setElement(A, 22, 43, 2);
  setElement(A, 22, 44, 2);
  setElement(A, 22, 45, 1);
  setElement(A, 22, 46, 1);
  //column 19
  setElement(A, 23, 5, 1);
  setElement(A, 23, 6, 1);
  setElement(A, 23, 7, 1);
  setElement(A, 23, 8, 2);
  setElement(A, 23, 9, 2);
  setElement(A, 23, 10, 2);
  setElement(A, 23, 13, 1);
  setElement(A, 23, 21, 2);
  setElement(A, 23, 22, 2);
  setElement(A, 23, 23, 2);
  setElement(A, 23, 24, 1);
  setElement(A, 23, 25, 1);
  setElement(A, 23, 26, 1);
  setElement(A, 23, 27, 1);
  setElement(A, 23, 28, 1);
  setElement(A, 23, 29, 2);
  setElement(A, 23, 30, 2);
  setElement(A, 23, 31, 1);
  setElement(A, 23, 32, 1);
  setElement(A, 23, 33, 1);
  setElement(A, 23, 37, 1);
  setElement(A, 23, 38, 1);
  setElement(A, 23, 39, 1);
  setElement(A, 23, 40, 1);
  setElement(A, 23, 41, 2);
  setElement(A, 23, 44, 1);
  setElement(A, 23, 45, 1);
  setElement(A, 23, 46, 1);
  //column 20
  setElement(A, 24, 5, 1);
  setElement(A, 24, 6, 2);
  setElement(A, 24, 7, 2);
  setElement(A, 24, 8, 2);
  setElement(A, 24, 9, 2);
  setElement(A, 24, 10, 2);
  setElement(A, 24, 12, 1);
  setElement(A, 24, 13, 1);
  setElement(A, 24, 14, 1);
  setElement(A, 24, 20, 2);
  setElement(A, 24, 21, 2);
  setElement(A, 24, 22, 2);
  setElement(A, 24, 23, 2);
  setElement(A, 24, 24, 1);
  setElement(A, 24, 25, 1);
  setElement(A, 24, 26, 2);
  setElement(A, 24, 27, 2);
  setElement(A, 24, 28, 2);
  setElement(A, 24, 29, 2);
  setElement(A, 24, 32, 1);
  setElement(A, 24, 33, 1);
  setElement(A, 24, 37, 1);
  setElement(A, 24, 38, 1);
  setElement(A, 24, 39, 1);
  setElement(A, 24, 43, 1);
  setElement(A, 24, 44, 1);
  setElement(A, 24, 45, 1);
  setElement(A, 24, 46, 1);
  //column 21
  setElement(A, 25, 5, 1);
  setElement(A, 25, 9, 2);
  setElement(A, 25, 10, 2);
  setElement(A, 25, 11, 2);
  setElement(A, 25, 12, 1);
  setElement(A, 25, 13, 1);
  setElement(A, 25, 14, 1);
  setElement(A, 25, 15, 1);
  setElement(A, 25, 19, 1);
  setElement(A, 25, 20, 1);
  setElement(A, 25, 21, 2);
  setElement(A, 25, 23, 1);
  setElement(A, 25, 25, 2);
  setElement(A, 25, 26, 2);
  setElement(A, 25, 27, 2);
  setElement(A, 25, 28, 2);
  setElement(A, 25, 29, 2);
  setElement(A, 25, 36, 1);
  setElement(A, 25, 37, 1);
  setElement(A, 25, 38, 1);
  setElement(A, 25, 42, 1);
  setElement(A, 25, 43, 2);
  //column 22
  setElement(A, 26, 10, 1);
  setElement(A, 26, 11, 1);
  setElement(A, 26, 12, 1);
  setElement(A, 26, 13, 1);
  setElement(A, 26, 14, 1);
  setElement(A, 26, 15, 1);
  setElement(A, 26, 16, 2);
  setElement(A, 26, 17, 2);
  setElement(A, 26, 18, 1);
  setElement(A, 26, 19, 1);
  setElement(A, 26, 20, 1);
  setElement(A, 26, 21, 2);
  setElement(A, 26, 25, 2);
  setElement(A, 26, 26, 2);
  setElement(A, 26, 27, 2);
  setElement(A, 26, 28, 2);
  setElement(A, 26, 29, 1);
  setElement(A, 26, 35, 1);
  setElement(A, 26, 36, 2);
  setElement(A, 26, 42, 1);
  setElement(A, 26, 43, 2);
  setElement(A, 26, 44, 2);
  setElement(A, 26, 45, 2);
  setElement(A, 26, 46, 1);
  //column 23
  setElement(A, 27, 9, 1);
  setElement(A, 27, 10, 1);
  setElement(A, 27, 11, 1);
  setElement(A, 27, 12, 1);
  setElement(A, 27, 13, 1);
  setElement(A, 27, 14, 2);
  setElement(A, 27, 15, 2);
  setElement(A, 27, 16, 2);
  setElement(A, 27, 17, 2);
  setElement(A, 27, 18, 1);
  setElement(A, 27, 19, 1);
  setElement(A, 27, 20, 1);
  setElement(A, 27, 24, 2);
  setElement(A, 27, 25, 2);
  setElement(A, 27, 26, 2);
  setElement(A, 27, 27, 1);
  setElement(A, 27, 28, 1);
  setElement(A, 27, 29, 1);
  setElement(A, 27, 30, 1);
  setElement(A, 27, 34, 1);
  setElement(A, 27, 35, 2);
  setElement(A, 27, 36, 2);
  setElement(A, 27, 44, 2);
  setElement(A, 27, 45, 2);
  setElement(A, 27, 46, 1);
  //column 24
  setElement(A, 28, 5, 1);
  setElement(A, 28, 11, 1);
  setElement(A, 28, 12, 1);
  setElement(A, 28, 14, 2);
  setElement(A, 28, 15, 2);
  setElement(A, 28, 16, 2);
  setElement(A, 28, 17, 2);
  setElement(A, 28, 18, 1);
  setElement(A, 28, 23, 1);
  setElement(A, 28, 24, 2);
  setElement(A, 28, 25, 2);
  setElement(A, 28, 29, 1);
  setElement(A, 28, 30, 1);
  setElement(A, 28, 31, 1);
  setElement(A, 28, 32, 1);
  setElement(A, 28, 33, 1);
  setElement(A, 28, 34, 2);
  setElement(A, 28, 35, 2);
  setElement(A, 28, 36, 2);
  setElement(A, 28, 37, 2);
  setElement(A, 28, 44, 1);
  setElement(A, 28, 45, 1);
  setElement(A, 28, 46, 1);
  //column 25
  setElement(A, 29, 5, 1);
  setElement(A, 29, 6, 1);
  setElement(A, 29, 12, 1);
  setElement(A, 29, 13, 2);
  setElement(A, 29, 14, 2);
  setElement(A, 29, 15, 2);
  setElement(A, 29, 16, 2);
  setElement(A, 29, 17, 1);
  setElement(A, 29, 24, 1);
  setElement(A, 29, 25, 1);
  setElement(A, 29, 30, 1);
  setElement(A, 29, 31, 1);
  setElement(A, 29, 32, 1);
  setElement(A, 29, 35, 2);
  setElement(A, 29, 36, 2);
  setElement(A, 29, 37, 1);
  setElement(A, 29, 38, 2);
  setElement(A, 29, 42, 1);
  setElement(A, 29, 43, 1);
  setElement(A, 29, 44, 2);
  setElement(A, 29, 45, 1);
  setElement(A, 29, 46, 1);
  //column 26
  setElement(A, 30, 5, 1);
  setElement(A, 30, 6, 1);
  setElement(A, 30, 7, 1);
  setElement(A, 30, 8, 1);
  setElement(A, 30, 12, 1);
  setElement(A, 30, 13, 2);
  setElement(A, 30, 14, 2);
  setElement(A, 30, 15, 2);
  setElement(A, 30, 16, 2);
  setElement(A, 30, 22, 1);
  setElement(A, 30, 26, 1);
  setElement(A, 30, 31, 1);
  setElement(A, 30, 32, 1);
  setElement(A, 30, 35, 2);
  setElement(A, 30, 36, 1);
  setElement(A, 30, 37, 1);
  setElement(A, 30, 38, 1);
  setElement(A, 30, 39, 2);
  setElement(A, 30, 40, 2);
  setElement(A, 30, 43, 2);
  setElement(A, 30, 44, 2);
  setElement(A, 30, 45, 2);
  setElement(A, 30, 46, 1);
  //column 27
  setElement(A, 31, 5, 1);
  setElement(A, 31, 6, 1);
  setElement(A, 31, 7, 2);
  setElement(A, 31, 8, 2);
  setElement(A, 31, 11, 1);
  setElement(A, 31, 12, 1);
  setElement(A, 31, 14, 2);
  setElement(A, 31, 15, 2);
  setElement(A, 31, 16, 2);
  setElement(A, 31, 21, 1);
  setElement(A, 31, 22, 1);
  setElement(A, 31, 23, 1);
  setElement(A, 31, 26, 1);
  setElement(A, 31, 31, 1);
  setElement(A, 31, 32, 1);
  setElement(A, 31, 35, 1);
  setElement(A, 31, 36, 1);
  setElement(A, 31, 37, 1);
  setElement(A, 31, 38, 2);
  setElement(A, 31, 39, 2);
  setElement(A, 31, 40, 2);
  setElement(A, 31, 41, 2);
  setElement(A, 31, 42, 2);
  setElement(A, 31, 43, 2);
  setElement(A, 31, 44, 2);
  //column 28
  setElement(A, 32, 5, 1);
  setElement(A, 32, 6, 1);
  setElement(A, 32, 7, 2);
  setElement(A, 32, 8, 2);
  setElement(A, 32, 9, 2);
  setElement(A, 32, 10, 1);
  setElement(A, 32, 11, 1);
  setElement(A, 32, 15, 2);
  setElement(A, 32, 16, 2);
  setElement(A, 32, 18, 1);
  setElement(A, 32, 23, 1);
  setElement(A, 32, 24, 1);
  setElement(A, 32, 25, 1);
  setElement(A, 32, 30, 1);
  setElement(A, 32, 31, 1);
  setElement(A, 32, 32, 1);
  setElement(A, 32, 35, 1);
  setElement(A, 32, 36, 1);
  setElement(A, 32, 37, 1);
  setElement(A, 32, 38, 1);
  setElement(A, 32, 39, 2);
  setElement(A, 32, 40, 2);
  setElement(A, 32, 41, 2);
  setElement(A, 32, 42, 2);
  setElement(A, 32, 43, 1);
  setElement(A, 32, 44, 1);
  //column 29
  setElement(A, 33, 5, 1);
  setElement(A, 33, 7, 2);
  setElement(A, 33, 8, 2);
  setElement(A, 33, 9, 2);
  setElement(A, 33, 10, 2);
  setElement(A, 33, 11, 2);
  setElement(A, 33, 12, 1);
  setElement(A, 33, 13, 1);
  setElement(A, 33, 14, 1);
  setElement(A, 33, 15, 2);
  setElement(A, 33, 17, 1);
  setElement(A, 33, 18, 1);
  setElement(A, 33, 19, 1);
  setElement(A, 33, 24, 1);
  setElement(A, 33, 25, 1);
  setElement(A, 33, 29, 1);
  setElement(A, 33, 30, 1);
  setElement(A, 33, 31, 1);
  setElement(A, 33, 35, 2);
  setElement(A, 33, 36, 1);
  setElement(A, 33, 37, 1);
  setElement(A, 33, 38, 1);
  setElement(A, 33, 39, 1);
  setElement(A, 33, 41, 2);
  setElement(A, 33, 42, 1);
  setElement(A, 33, 43, 1);
  //column 30
  setElement(A, 34, 8, 2);
  setElement(A, 34, 9, 2);
  setElement(A, 34, 10, 2);
  setElement(A, 34, 11, 2);
  setElement(A, 34, 12, 1);
  setElement(A, 34, 13, 1);
  setElement(A, 34, 14, 1);
  setElement(A, 34, 18, 1);
  setElement(A, 34, 19, 1);
  setElement(A, 34, 20, 1);
  setElement(A, 34, 23, 2);
  setElement(A, 34, 24, 2);
  setElement(A, 34, 25, 1);
  setElement(A, 34, 26, 1);
  setElement(A, 34, 30, 1);
  setElement(A, 34, 34, 2);
  setElement(A, 34, 35, 2);
  setElement(A, 34, 36, 1);
  setElement(A, 34, 37, 1);
  setElement(A, 34, 38, 1);
  setElement(A, 34, 39, 1);
  setElement(A, 34, 40, 1);
  setElement(A, 34, 42, 1);
  setElement(A, 34, 43, 1);
  //column 31
  setElement(A, 35, 6, 1);
  setElement(A, 35, 7, 1);
  setElement(A, 35, 9, 2);
  setElement(A, 35, 10, 2);
  setElement(A, 35, 11, 1);
  setElement(A, 35, 12, 1);
  setElement(A, 35, 13, 1);
  setElement(A, 35, 14, 1);
  setElement(A, 35, 18, 1);
  setElement(A, 35, 19, 1);
  setElement(A, 35, 22, 2);
  setElement(A, 35, 23, 2);
  setElement(A, 35, 24, 2);
  setElement(A, 35, 25, 1);
  setElement(A, 35, 26, 1);
  setElement(A, 35, 28, 2);
  setElement(A, 35, 31, 1);
  setElement(A, 35, 32, 2);
  setElement(A, 35, 33, 2);
  setElement(A, 35, 34, 2);
  setElement(A, 35, 35, 2);
  setElement(A, 35, 36, 2);
  setElement(A, 35, 37, 2);
  setElement(A, 35, 38, 1);
  setElement(A, 35, 41, 1);
  setElement(A, 35, 42, 1);
  //column 32
  setElement(A, 36, 5, 1);
  setElement(A, 36, 6, 1);
  setElement(A, 36, 7, 1);
  setElement(A, 36, 12, 1);
  setElement(A, 36, 13, 1);
  setElement(A, 36, 14, 1);
  setElement(A, 36, 15, 1);
  setElement(A, 36, 19, 1);
  setElement(A, 36, 21, 2);
  setElement(A, 36, 22, 2);
  setElement(A, 36, 23, 1);
  setElement(A, 36, 26, 1);
  setElement(A, 36, 27, 1);
  setElement(A, 36, 28, 2);
  setElement(A, 36, 29, 2);
  setElement(A, 36, 31, 1);
  setElement(A, 36, 32, 1);
  setElement(A, 36, 33, 2);
  setElement(A, 36, 34, 2);
  setElement(A, 36, 35, 2);
  setElement(A, 36, 36, 2);
  setElement(A, 36, 37, 2);
  setElement(A, 36, 38, 2);
  setElement(A, 36, 39, 1);
  setElement(A, 36, 46, 1);
  //column 33
  setElement(A, 37, 5, 1);
  setElement(A, 37, 11, 1);
  setElement(A, 37, 12, 1);
  setElement(A, 37, 19, 1);
  setElement(A, 37, 20, 1);
  setElement(A, 37, 21, 2);
  setElement(A, 37, 22, 2);
  setElement(A, 37, 23, 1);
  setElement(A, 37, 27, 1);
  setElement(A, 37, 28, 2);
  setElement(A, 37, 29, 2);
  setElement(A, 37, 30, 1);
  setElement(A, 37, 31, 1);
  setElement(A, 37, 32, 1);
  setElement(A, 37, 33, 1);
  setElement(A, 37, 34, 1);
  setElement(A, 37, 35, 2);
  setElement(A, 37, 36, 2);
  setElement(A, 37, 37, 2);
  setElement(A, 37, 38, 2);
  setElement(A, 37, 39, 2);
  setElement(A, 37, 40, 1);
  setElement(A, 37, 46, 1);
  //column 34
  setElement(A, 38, 5, 1);
  setElement(A, 38, 9, 1);
  setElement(A, 38, 10, 1);
  setElement(A, 38, 11, 1);
  setElement(A, 38, 12, 1);
  setElement(A, 38, 18, 1);
  setElement(A, 38, 19, 1);
  setElement(A, 38, 20, 1);
  setElement(A, 38, 21, 2);
  setElement(A, 38, 22, 2);
  setElement(A, 38, 23, 1);
  setElement(A, 38, 24, 1);
  setElement(A, 38, 27, 1);
  setElement(A, 38, 28, 2);
  setElement(A, 38, 30, 1);
  setElement(A, 38, 31, 1);
  setElement(A, 38, 32, 2);
  setElement(A, 38, 33, 2);
  setElement(A, 38, 34, 1);
  setElement(A, 38, 35, 1);
  setElement(A, 38, 36, 1);
  setElement(A, 38, 37, 2);
  setElement(A, 38, 38, 2);
  setElement(A, 38, 39, 2);
  setElement(A, 38, 40, 1);
  setElement(A, 38, 41, 1);
  setElement(A, 38, 45, 1);
  setElement(A, 38, 46, 1);
  //column 35
  setElement(A, 39, 5, 2);
  setElement(A, 39, 8, 1);
  setElement(A, 39, 9, 1);
  setElement(A, 39, 10, 1);
  setElement(A, 39, 17, 1);
  setElement(A, 39, 18, 1);
  setElement(A, 39, 19, 1);
  setElement(A, 39, 20, 2);
  setElement(A, 39, 21, 2);
  setElement(A, 39, 24, 1);
  setElement(A, 39, 26, 2);
  setElement(A, 39, 27, 1);
  setElement(A, 39, 28, 2);
  setElement(A, 39, 29, 1);
  setElement(A, 39, 31, 2);
  setElement(A, 39, 32, 2);
  setElement(A, 39, 33, 2);
  setElement(A, 39, 34, 2);
  setElement(A, 39, 35, 1);
  setElement(A, 39, 36, 1);
  setElement(A, 39, 37, 2);
  setElement(A, 39, 38, 2);
  setElement(A, 39, 39, 2);
  setElement(A, 39, 40, 1);
  setElement(A, 39, 41, 1);
  setElement(A, 39, 42, 1);
  setElement(A, 39, 43, 1);
  setElement(A, 39, 44, 1);
  setElement(A, 39, 45, 1);
  setElement(A, 39, 46, 1);
  //column 36
  setElement(A, 40, 5, 2);
  setElement(A, 40, 6, 2);
  setElement(A, 40, 9, 1);
  setElement(A, 40, 15, 1);
  setElement(A, 40, 16, 1);
  setElement(A, 40, 17, 1);
  setElement(A, 40, 18, 1);
  setElement(A, 40, 20, 1);
  setElement(A, 40, 21, 2);
  setElement(A, 40, 26, 2);
  setElement(A, 40, 27, 1);
  setElement(A, 40, 28, 1);
  setElement(A, 40, 31, 2);
  setElement(A, 40, 32, 2);
  setElement(A, 40, 33, 2);
  setElement(A, 40, 36, 1);
  setElement(A, 40, 37, 1);
  setElement(A, 40, 38, 2);
  setElement(A, 40, 39, 2);
  setElement(A, 40, 40, 2);
  setElement(A, 40, 42, 1);
  setElement(A, 40, 43, 1);
  setElement(A, 40, 44, 1);
  setElement(A, 40, 45, 1);
  //column 37
  setElement(A, 41, 5, 1);
  setElement(A, 41, 6, 2);
  setElement(A, 41, 7, 2);
  setElement(A, 41, 8, 2);
  setElement(A, 41, 9, 2);
  setElement(A, 41, 10, 1);
  setElement(A, 41, 13, 2);
  setElement(A, 41, 14, 2);
  setElement(A, 41, 15, 2);
  setElement(A, 41, 16, 2);
  setElement(A, 41, 17, 2);
  setElement(A, 41, 18, 1);
  setElement(A, 41, 19, 1);
  setElement(A, 41, 20, 2);
  setElement(A, 41, 21, 1);
  setElement(A, 41, 22, 1);
  setElement(A, 41, 25, 2);
  setElement(A, 41, 26, 2);
  setElement(A, 41, 27, 2);
  setElement(A, 41, 28, 2);
  setElement(A, 41, 30, 2);
  setElement(A, 41, 31, 2);
  setElement(A, 41, 32, 1);
  setElement(A, 41, 33, 2);
  setElement(A, 41, 36, 1);
  setElement(A, 41, 37, 1);
  setElement(A, 41, 38, 2);
  setElement(A, 41, 39, 2);
  setElement(A, 41, 42, 1);
  setElement(A, 41, 44, 1);
  setElement(A, 41, 45, 1);
  setElement(A, 41, 46, 1);
  //column 38
  setElement(A, 42, 5, 1);
  setElement(A, 42, 6, 2);
  setElement(A, 42, 7, 2);
  setElement(A, 42, 10, 1);
  setElement(A, 42, 11, 1);
  setElement(A, 42, 12, 2);
  setElement(A, 42, 13, 2);
  setElement(A, 42, 14, 2);
  setElement(A, 42, 15, 2);
  setElement(A, 42, 16, 1);
  setElement(A, 42, 17, 1);
  setElement(A, 42, 18, 2);
  setElement(A, 42, 19, 2);
  setElement(A, 42, 22, 1);
  setElement(A, 42, 23, 1);
  setElement(A, 42, 25, 2);
  setElement(A, 42, 26, 2);
  setElement(A, 42, 27, 2);
  setElement(A, 42, 28, 1);
  setElement(A, 42, 29, 1);
  setElement(A, 42, 30, 1);
  setElement(A, 42, 31, 1);
  setElement(A, 42, 32, 1);
  setElement(A, 42, 33, 2);
  setElement(A, 42, 36, 1);
  setElement(A, 42, 37, 1);
  setElement(A, 42, 45, 1);
  setElement(A, 42, 46, 1);
  //column 39
  setElement(A, 43, 5, 2);
  setElement(A, 43, 6, 2);
  setElement(A, 43, 7, 1);
  setElement(A, 43, 11, 1);
  setElement(A, 43, 12, 1);
  setElement(A, 43, 13, 2);
  setElement(A, 43, 14, 2);
  setElement(A, 43, 15, 2);
  setElement(A, 43, 16, 2);
  setElement(A, 43, 17, 2);
  setElement(A, 43, 22, 1);
  setElement(A, 43, 23, 1);
  setElement(A, 43, 24, 1);
  setElement(A, 43, 25, 1);
  setElement(A, 43, 26, 2);
  setElement(A, 43, 27, 2);
  setElement(A, 43, 28, 2);
  setElement(A, 43, 29, 1);
  setElement(A, 43, 30, 1);
  setElement(A, 43, 31, 2);
  setElement(A, 43, 32, 2);
  setElement(A, 43, 33, 2);
  setElement(A, 43, 35, 1);
  setElement(A, 43, 36, 1);
  setElement(A, 43, 37, 1);
  setElement(A, 43, 38, 1);
  setElement(A, 43, 46, 1);
  //column 40
  setElement(A, 44, 5, 2);
  setElement(A, 44, 7, 1);
  setElement(A, 44, 8, 1);
  setElement(A, 44, 13, 1);
  setElement(A, 44, 14, 2);
  setElement(A, 44, 15, 2);
  setElement(A, 44, 16, 2);
  setElement(A, 44, 23, 1);
  setElement(A, 44, 24, 1);
  setElement(A, 44, 25, 1);
  setElement(A, 44, 26, 1);
  setElement(A, 44, 27, 2);
  setElement(A, 44, 28, 2);
  setElement(A, 44, 29, 1);
  setElement(A, 44, 30, 2);
  setElement(A, 44, 31, 2);
  setElement(A, 44, 32, 2);
  setElement(A, 44, 37, 1);
  setElement(A, 44, 38, 1);
  setElement(A, 44, 39, 1);
  //column 41
  setElement(A, 45, 5, 1);
  setElement(A, 45, 12, 1);
  setElement(A, 45, 13, 1);
  setElement(A, 45, 14, 1);
  setElement(A, 45, 15, 1);
  setElement(A, 45, 16, 2);
  setElement(A, 45, 17, 1);
  setElement(A, 45, 20, 1);
    setElement(A, 45, 25, 4);
  setElement(A, 45, 26, 1);
  setElement(A, 45, 27, 2);
  setElement(A, 45, 28, 2);
  setElement(A, 45, 29, 2);
  setElement(A, 45, 30, 2);
  setElement(A, 45, 31, 2);
  setElement(A, 45, 32, 1);
  setElement(A, 45, 33, 1);
    setElement(A, 45, 36, 2);
  setElement(A, 45, 38, 1);
  setElement(A, 45, 39, 1);
  setElement(A, 45, 40, 1);
  //column 42
  setElement(A, 46, 5, 1);
  setElement(A, 46, 6, 1);
  setElement(A, 46, 10, 1);
  setElement(A, 46, 11, 1);
  setElement(A, 46, 12, 1);
  setElement(A, 46, 13, 1);
  setElement(A, 46, 14, 1);
  setElement(A, 46, 15, 1);
  setElement(A, 46, 16, 1);
  setElement(A, 46, 17, 1);
  setElement(A, 46, 19, 1);
  setElement(A, 46, 20, 1);
  setElement(A, 46, 21, 1);
  setElement(A, 46, 24, 1);
  setElement(A, 46, 25, 1);
  setElement(A, 46, 26, 1);
  setElement(A, 46, 27, 1);
  setElement(A, 46, 28, 2);
  setElement(A, 46, 29, 2);
  setElement(A, 46, 30, 2);
  setElement(A, 46, 31, 1);
  setElement(A, 46, 32, 1);
  setElement(A, 46, 33, 1);
  setElement(A, 46, 34, 1);
  setElement(A, 46, 37, 1);
  setElement(A, 46, 38, 1);
  setElement(A, 46, 39, 1);
  setElement(A, 46, 40, 1);
  setElement(A, 46, 41, 1);
  setElement(A, 46, 42, 1);
  setElement(A, 46, 46, 5);
*/

  //setElement(P, 1, 1, 5);
 // setElement(P, 1, 2, 5);
  end = 0; 
  int contra = 0;
  while(end !=1 && c != '.'){
    printMatrix(A, P, 0);
    c = getchar();
    if(c == 'd' || c == 'D'){
      if(contra == 5 || contra == 7) contra++;
      //Contra code initialization
      else contra = 0;
      setElement(P, 1, 3, 2);
      x = getElement(A, getElement(P, 1, 1) + 1, getElement(P, 1, 2));
      //moving right
      if(x == 0) setElement(P, 1, 1, getElement(P, 1, 1) + 1);
      if(x == 5) setElement(P, 1, 1, getElement(P, 1, 1) + 1);
      if(x == 3 || x == 4){
        setElement(P, 1, 1, getElement(P, 1, 1) + 1);
        setElement(A, getElement(P, 1, 1), getElement(P, 1, 2), 0);
        printMatrix(A, P, 8);
        if(x == 3){
          //moving right into a potion
          printf("      Found a Potion!      |\n\r");
          setElement(P, 1, 6, getElement(P, 1, 6) + 1);
        }
        else{
          //moving right into a Super potion
          printf("   Found a Super Potion!   |\n\r");
          setElement(P, 1, 7, getElement(P, 1, 7) + 1);
        }
        printf("+---------------------------+\n\r");
        c = getchar();
      }
      if(x == 2){
        //moving right into grass
        setElement(P, 1, 1, getElement(P, 1, 1) + 1);
        p = grass(A, P, x);
      }
    }
    if(c == 's' || c == 'S'){
      //moving down
      if(contra == 2 || contra == 3) contra++;
      else contra = 0;
      setElement(P, 1, 3, 3);
      x = getElement(A, getElement(P, 1, 1), getElement(P, 1, 2) + 1);
      if(x == 0) setElement(P, 1, 2, getElement(P, 1, 2) + 1);
      if(x == 5) setElement(P, 1, 2, getElement(P, 1, 2) + 1);
      if(x == 3 || x == 4){
        setElement(P, 1, 2, getElement(P, 1, 2) + 1);
        setElement(A, getElement(P, 1, 1), getElement(P, 1, 2), 0);
        printMatrix(A, P, 8);
        if(x == 3){
          //moving down into a potion
          printf("      Found a Potion!      |\n\r");
          setElement(P, 1, 6, getElement(P, 1, 6) + 1);
        }
        else{
          //moving down into a super potion
          printf("   Found a Super Potion!   |\n\r");
          setElement(P, 1, 7, getElement(P, 1, 7) + 1);
        }
        printf("+---------------------------+\n\r");
        c = getchar();
      }
      if(x == 2){
        //moving down into grass
        setElement(P, 1, 2, getElement(P, 1, 2) + 1);
        p = grass(A, P, x);
      }
    }
    if(c == 'a' || c == 'A'){
      //moving left
      if(contra == 4 || contra == 6) contra++;
      else contra = 0;
      setElement(P, 1, 3, 4);
      x = getElement(A, getElement(P, 1, 1) - 1, getElement(P, 1, 2));
      if(x == 0) setElement(P, 1, 1, getElement(P, 1, 1) - 1);
      if(x == 5) setElement(P, 1, 1, getElement(P, 1, 1) - 1);
      if(x == 3 || x == 4){
        setElement(P, 1, 1, getElement(P, 1, 1) - 1);
        setElement(A, getElement(P, 1, 1), getElement(P, 1, 2), 0);
        printMatrix(A, P, 8);
        if(x == 3){
          //moving left into a potion
          printf("      Found a Potion!      |\n\r");
          setElement(P, 1, 6, getElement(P, 1, 6) + 1);
        }
        else{
          //moving left into a super potion
          printf("   Found a Super Potion!   |\n\r");
          setElement(P, 1, 7, getElement(P, 1, 7) + 1);
        }
        printf("+---------------------------+\n\r");
        c = getchar();
      }
      if(x == 2){
        //moving left into grass
        setElement(P, 1, 1, getElement(P, 1, 1) - 1);
        p = grass(A, P, x);
      }
    }
    if(c == 'w' || c == 'W'){
      //moving up
      if (contra == 0 || contra == 1) contra++;
      else contra = 0;
      setElement(P, 1, 3, 1);
      x = getElement(A, getElement(P, 1, 1), getElement(P, 1, 2) - 1);
      if(x == 0) setElement(P, 1, 2, getElement(P, 1, 2) - 1);
      if(x == 5) setElement(P, 1, 2, getElement(P, 1, 2) - 1);
      if(x == 3 || x == 4){
        setElement(P, 1, 2, getElement(P, 1, 2) - 1);
        setElement(A, getElement(P, 1, 1), getElement(P, 1, 2), 0);
        printMatrix(A, P, 8);
        if(x == 3){
          //moving up into a potion
          printf("      Found a Potion!      |\n\r");
          setElement(P, 1, 6, getElement(P, 1, 6) + 1);
        }
        else{
          //moving up into a super potion
          printf("   Found a Super Potion!   |\n\r");
          setElement(P, 1, 7, getElement(P, 1, 7) + 1);
        }
        printf("+---------------------------+\n\r");
        c = getchar();
      }
      if(x == 2){
        //move up into grass
        setElement(P, 1, 2, getElement(P, 1, 2) - 1);
        p = grass(A, P, x);
      }
    }
    if(c == 'e' || c == 'E'){
      contra = 0;
      item(A, P);
    }
    if(c == 'r' || c == 'R'){
      //pokemon info
      contra = 0;
      printMatrix(A, P, 7);
      printf(" Your Pokemon:   Dragonair |\n\r");
      printf("|                    Lvl %d |\n\r", getElement(P, 1, 13));
      printf("|                ");
      if(getElement(P, 1, 4) < 100){
        if(getElement(P, 1, 4) < 10) printf("  ");
        else printf(" ");
      }
      printf("HP %d/%d |\n\r", getElement(P, 1, 4), getElement(P, 1, 5));
      printf("+---------------------------+\n\r");
      //any key closes
      c = getchar();
    }
    if(contra == 8){
      c = getchar();
      if(c == 'b'){
        c = getchar();
        if(c == 'a'){
          //Because what game is complete without a Contra Code??
          printf("+---------------------------+\n\r");
          printf("|~~~~~~~~~        ~~~~~~~~~~|\n\r");
          printf("|~~~~~                ~~~~~~|\n\r");
          printf("|~~~                    ~~~~|\n\r");
          printf("|~~                      ~~~|\n\r");
          printf("|~~ CONTRA CODE DETECTED ~~~|\n\r");
          printf("|~~                      ~~~|\n\r");
          printf("|~~~                    ~~~~|\n\r");
          printf("|~~~~~                ~~~~~~|\n\r");
          printf("|~~~~~~~~~        ~~~~~~~~~~|\n\r");
          printf("+---------------------------+\n\r");
          c = getchar();
          //Forces s to be one in all encounters, making every enemy shiny
          setElement(P, 1, 15, 1);
        }
      }
    }
    if(getElement(P, 1, 4) == 0 ){
      printf("+---------------------------+\n\r");
      printf("|                           |\n\r");
      printf("|                           |\n\r");
      printf("|         Game Over         |\n\r");
      printf("|                           |\n\r");
      printf("|                           |\n\r");
      printf("|                           |\n\r");
      printf("|                           |\n\r");
      printf("|                           |\n\r");
      printf("|       Press any key       |\n\r");
      printf("+---------------------------+\n\r");
      c = getchar();
      end = 1;
      // Rekt. Scrub.
    }
    if(getElement(A, getElement(P, 1, 1), getElement(P, 1, 2)) == 5){
      printf("+---------------------------+\n\r");
      printf("|                           |\n\r");
      printf("|      Congraturation!      |\n\r");
      printf("|                           |\n\r");
      printf("|  This game is happy end.  |\n\r");
      printf("|                           |\n\r");
      printf("|                           |\n\r");
      printf("|                           |\n\r");
      printf("|                           |\n\r");
      printf("|       Press any key       |\n\r");
      printf("+---------------------------+\n\r");
      //reference to Ghosts 'n Goblins
      c = getchar();
      end = 1;
      score = 2500;
    }
  }
 score = score +(getElement(P, 1, 13) - 40)*100;
 score = score + getElement(P, 1, 4);
 score = score + getElement(P, 1, 8)*10;
 score = score + getElement(P, 1, 10);
 score = score + getElement(P, 1, 11)*1000;
 score = score + getElement(P, 1, 12)*750;

if(score > hiscr){
  if (map == 0){
    fp = fopen("lmap.txt", "r+");
rewind(fp);
    fprintf(fp, "%d %d", dim, score);
    fclose(fp);
  }
  if (map == 1){
    fp = fopen("save1.txt", "r+");
rewind(fp);
    fprintf(fp, "%d %d", dim, score);
    fclose(fp);
  }
  if (map == 2){
    fp = fopen("save2.txt", "r+");
rewind(fp);
    fprintf(fp, "%d %d", dim, score);
    fclose(fp);
  }
  if (map == 3){
    fp = fopen("save3.txt", "r+");
rewind(fp);
    fprintf(fp, "%d %d", dim, score);
    fclose(fp);
  }
// high score of 271956

      printf("+---------------------------+\n\r");
      printf("|                           |\n\r");
      printf("|      New High Score!      |\n\r");
      printf("|          ");
      if(score < 1000) printf(" ");
      if(score < 100000) printf(" ");
      printf("%d           ", score);
      if(score < 10000) printf(" ");
      printf("|\n\r");
      printf("|                           |\n\r");
      printf("|      Previous Best:       |\n\r");
      printf("|          ");
      if(hiscr < 10) printf(" ");
      if(hiscr < 1000) printf(" ");
      if(hiscr < 100000) printf(" ");
      printf("%d           ", hiscr);
      if(hiscr < 10000) printf(" ");
      if(hiscr < 100) printf(" ");
      printf("|\n\r");
      printf("|                           |\n\r"); 
      printf("|                           |\n\r");
  }
  else{
      printf("+---------------------------+\n\r");
      printf("|                           |\n\r");
      printf("|        High Score:        |\n\r");
      
      printf("|          ");
      if(hiscr < 1000) printf(" ");
      if(hiscr < 100000) printf(" ");
      printf("%d           ", hiscr);
      if(hiscr < 10000) printf(" ");
      printf("|\n\r");
      
      printf("|                           |\n\r");
      printf("|          Score:           |\n\r");
      printf("|          ");
      if(score < 1000) printf(" ");
      if(score < 100000) printf(" ");
      printf("%d           ", score);
      if(score < 10000) printf(" ");
      printf("|\n\r");
      printf("|                           |\n\r");
      printf("|                           |\n\r");
      }
      
      printf("|       Press any key       |\n\r");
      printf("+---------------------------+\n\r");
  c = getchar();
  //really ugly looking bunch of code to format the end game stats and make it look pretty.
  printf("+---------------------------+\n\r");
  printf("|           Stats:          |\n\r");
  printf("| Dragonair's Level      %d |\n\r", getElement(P, 1, 13));
  //Level domain = 0 - 99
  printf("| Dragonair's HP        ");
  //hp domain = 0 - 999
  if(getElement(P, 1, 4) < 100){
    if (getElement(P, 1, 4) < 10) printf("  ");
    else printf(" ");
  }
  printf("%d |\n\r", getElement(P, 1, 4));
  printf("| Enemies KO'ed         ");
  // ko range = 0 - 999
  if(getElement(P, 1, 8) < 100){
    if (getElement(P, 1, 8) < 10) printf("  ");
    else printf(" ");
  }
  printf("%d |\n\r", getElement(P, 1, 8));
  printf("| Damage Dealt        ");
  //damage range = 0 - 99999
  if(getElement(P, 1, 10) < 10000){
    printf(" ");
    if(getElement(P, 1, 10) < 1000){
      if(getElement(P, 1, 10) < 100){
        if(getElement(P, 1, 10) < 10) printf("   ");
        else printf("  "); 
      }
      else printf(" ");
    }
  } 
  printf("%d |\n\r", getElement(P, 1, 10));
  printf("| Damage Taken         ");
  //damage range = 0 - 9999
  if(getElement(P, 1, 9) < 1000){
    if(getElement(P, 1, 9) < 100){
      if(getElement(P, 1, 9) < 10) printf("   ");
      else printf("  ");
    }
    else printf(" ");
  }
  printf("%d |\n\r", getElement(P, 1, 9));
  printf("| Shinies Fought        ");
  // shiny range = 0 - 999
  if(getElement(P, 1, 11) < 100){
    if (getElement(P, 1, 11) < 10) printf("  ");
    else printf(" ");
  }
  printf("%d |\n\r", getElement(P, 1, 11));
  //shows up as question marks if the player never summoned a Magikarp, 
  //otherwise shows up as Magikarp Fought
  if( getElement(P, 1, 12) == 0) printf("| ???????? Fought         0 |\n\r");
  else printf("| Magikarp Fought         %d |\n\r", getElement(P, 1, 12));
  //range = 0 - 9 because you won't need /get that many.
  printf("|       Press any key       |\n\r");
  printf("+---------------------------+\n\r");
  c = getchar();
  printf("+---------------------------+\n\r");
  printf("|                           |\n\r");
  printf("|                           |\n\r");
  printf("|                           |\n\r");
  printf("|    Thanks for playing!    |\n\r");
  printf("|                           |\n\r");
  printf("|                           |\n\r");
  printf("|                           |\n\r");
  printf("|                           |\n\r");
  printf("|       Press any key       |\n\r");
  printf("+---------------------------+\n\r");
  //game closes upon completion.
  c = getchar();
  system ("stty cooked echo");
  system ("/bin/stty cooked");
  deleteMatrix(A);
  deleteMatrix(P);
  return 0;
}
